module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            files: ['Gruntfile.js', 'src/js/**/*.js']
        },

        concat: {
            dist: {
                src: ['src/js/**/*.js'],
                dest: 'assets/js/main.js'
            }
        },

        uglify: {
            dist: {
                files: {
                    'assets/js/main.min.js': ['assets/js/main.js'],
                }
            },
            options: {
                mangle: true
            }
        },

        sass: {
            dist: {
                files: {
                    'assets/css/main.css': ['src/sass/main.scss']
                },
                options: {
                    outputStyle: ['expanded']
                }
            }
        },

        autoprefixer: {
            dist: {
                src: ['assets/css/main.css']
            },
            options: {
                browsers: ['last 2 versions']
            }
        },

        cssmin: {
            dist: {
                files: {
                    'assets/css/main.min.css': ['assets/css/main.css']
                }
            },
            options: {
                keepSpecialComments: 0
            }
        },

        watch: {
            scripts: {
                files: ['<%= jshint.files %>'],
                tasks: ['scripts'],
                options: {
                    spawn: false
                }
            },
            stylesheets: {
                files: ['src/sass/**/*.scss'],
                tasks: ['stylesheets'],
                options: {
                    spawn: false
                }
            }
        }
    });

    require('load-grunt-tasks')(grunt);
    grunt.registerTask('scripts', ['jshint', 'concat', 'uglify']);
    grunt.registerTask('stylesheets', ['sass', 'autoprefixer', 'cssmin']);
    grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'sass', 'autoprefixer', 'cssmin']);
};
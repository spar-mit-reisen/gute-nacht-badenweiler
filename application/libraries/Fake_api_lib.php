<?php

/**
 * User: sbraun
 * Date: 23.03.17
 * Time: 14:23
 */
class Fake_api_lib
{
    public function api_return($url_segments = [], array $get_params = [], array $post_params = []) {
        if (@ci()->config->item("use_fake_api")) {
            if ($url_segments[0] == "getTopic") {
                return $this->getTopic($url_segments[1], $url_segments[2]);
            }
            if ($url_segments[0] == "getMetatag") {
                return $this->getMetatag($url_segments[1], $url_segments[2], $url_segments[3], @$url_segments[4]);
            }
            if ($url_segments[0] == "getTopicPackages") {
                return $this->getTopicPackages($url_segments[1], @$url_segments[2], @$url_segments[3], @$url_segments[4]);
            }
            if ($url_segments[0] == "getTopicLinksByPackage") {
                return $this->getTopicLinksByPackage($url_segments[1], @$url_segments[2], @$url_segments[3], @$url_segments[4]);
            }
            if ($url_segments[0] == "getTopicTree") {
                return $this->getTopicTree(@$url_segments[1], @$url_segments[2], @$url_segments[3], @$url_segments[4]);
            }
            if ($url_segments[0] == "getPackageContent") {
//                return $this->getPackageContent(@$url_segments, $get_params, $post_params);
            }
            if ($url_segments[0] == "get_top_list") {
                return $this->get_top_list($url_segments[1]);
            }
            if ($url_segments[0] == "getPackage") {
                return $this->getPackage(@$url_segments[1], $get_params, $post_params);
            }


            #die(20);
//            error_log("Fake_api_lib does not implement method '$url_segments[0]'. Fall back to real api");
        }
        return ci()->api_lib()->api_return($url_segments, $get_params, $post_params);
    }

    public function getTopic($topic_class, $topic_name = "") {
        $this->data = [];
        $args = func_get_args();
        $url_segments = array_slice($args, 1);
        $this->data['url'] = $url = implode("/", $url_segments);
        $this->data['class'] = $topic_class;

        if (!empty($url) || $topic_class == "home") {
//            $this->load->model("cms/topic_model");
//            $this->load->model("cms/dogtag_knot_model");
//            $this->load->model("api/api_model");
//            $this->load->model("cms/picture_model");
//            $this->load->model("cms/old_picture_model");
//            $this->load->model("cms/gallery_model");
//            $this->load->model("cms/dogtag_model");
//            $this->load->model("cms/element_model");
//            /** @var Topic_model $topic_model */
            $topic_model = ci()->topic_model();
//            $topic_item = $topic_model->get_item_by_field("url", $url);
            $topic_item = $topic_model->get_item_by_class_and_url($topic_class, $url);
            if ($topic_item) {
                $parent1 = ci()->topic_model()->get_item_by_id($topic_item->parent_id);
                $topic_item->parent1 = $parent1;

                if (isset($parent1)) {
                    $parent2 = ci()->topic_model()->get_item_by_id($parent1->parent_id);
                    $topic_item->parent2 = $parent2;
                    if (isset($parent2)) {
                        $parent3 = ci()->topic_model()->get_item_by_id($parent2->parent_id);
                        $topic_item->parent3 = $parent3;
                    }
                    if (isset($parent3)) {
                        $parent4 = ci()->topic_model()->get_item_by_id($parent3->parent_id);
                        $topic_item->parent4 = $parent4;
                    }
                }
                ci()->api_model()->fill_topic($topic_item);

                $this->data['topic'] = $topic_item;
            }
        }
        return json_encode($this->data);
    }

    public function getMetatag($object_type = null, $object_id = null, $type = 'all') {
        $response = [];
        if (!empty($type) && !empty($object_type) && !empty($object_id) && in_array($type, ['all', 'description', 'title'])) {
            $response['request'] = [$type, $object_type, $object_id];
            if ($type == 'all')
                $types = ['title', 'description'];
            else
                $types = [$type];
            foreach ($types as $type) {
                $metatags_item = ci()->metatag_model()->get_item_by_object(['id' => $object_id], $object_type, $type);
                $response['metatags'][$type]/*['item']*/ = $metatags_item;
            }
        } else {
            $response['error'] = "Request is invalid!";
        }
        return json_encode($response);
    }

    public function getTopicPackages($topic_id, $return_type = "json", $limit_num = null) {
        $limit_from = (ci()->input->post_get("limit_from")) ? (int)ci()->input->post_get("limit_from") : 0;
        if (!is_null($limit_num)) {
            $limit_num = $limit_num;
        } else {
        $limit_num = (ci()->input->post_get("limit_num")) ? (int)ci()->input->post_get("limit_num") : ci()->package_api_model()->limit_num;
        }
        //TODO !!!
//        todo: get packages by hundemarke for topic
        $this->data = array();
//        $items = $this->controller_helper_lib->get_dogtag_list_items($topic_id, "topic");
//        $items = $this->controller_helper_lib()->get_items_by_dogtag_score($topic_id, "topic", "packages", $limit_num, $limit_from);
//        $params = ['limit_num' => $limit_num, 'limit_from' => $limit_from, 'return_type' => 'items'];
        $params = ['limit_num' => 0, 'limit_from' => 0, 'return_type' => 'items'];
//        $params = ['return_type' => 'items'];
//        $items = ci()->controller_helper_lib()->get_items_by_dogtag_score2($topic_id, "topic", "package", $params);
        $itemsCollection = ci()->package_model()->get_package_items_sorted_by_score_to_object3("topic", $topic_id, $params);
        $items = $itemsCollection->get_array($limit_num, $limit_from);
//        $params = ['limit_num' => $limit_num, 'limit_from' => $limit_from, 'return_type' => 'count'];
//        $count_items = ci()->controller_helper_lib()->get_items_by_dogtag_score2($topic_id, "topic", "package", $params);
//        $items = $this->controller_helper_lib()->get_items_by_dogtag_score($topic_id, "topic", "packages", 4, 1);#for test
        ci()->api_model()->fill_packages($items, $this->data['packages'], true);
//        $this->data["packages"] = $items;
//        $this->data['package_count'] = @$count_items[0]->count;
        $this->data['package_count'] = $itemsCollection->count();
        $this->data['request_info'] = ['limit_num' => $limit_num, 'limit_from' => $limit_from];
        if ($return_type == "json")
            return json_encode($this->data);
        else
            return ($this->data);
    }

    public function getTopicLinksByPackage($package_number = null) {
        $output = ci()->api_model()->get_topic_links_by_package((object) ['package_number' => $package_number]);
        return json_encode($output);
    }

    public function getTopicTree($show_hidden = false, $return_type = "json") {
        if ($return_type == "json")
            return json_encode(ci()->api_model()->get_topic_tree($show_hidden));
        else
            return (ci()->api_model()->get_topic_tree($show_hidden));
    }

    public function getPackageContent($url_segments, $get_params, $post_params) {
            $package_number_or_name = $url_segments[1];
            $contentType = @$url_segments[2];
            $count = 0;
            $type = array('content');
            if (isset($package_number_or_name)) {
                $data['json_output'] = ci()->api_model()->get_content_single($package_number_or_name, 'content', $contentType);
                return json_encode($data['json_output']);
        }
    }

    public function get_top_list($top_list_key = 'top-50-weekly') {
        $response = [];
        $top_list_data = ci()->api_model()->get_top_list_data($top_list_key);
        $response['top_list'] = $top_list_data;
        return json_encode($response);
    }


    public function getPackage($package_number, $get_params, $post_params) {
        if (is_null($package_number)) {
            echo "Paket-Nummer fehlt!";
            die;
        }
        ci()->load->model("cms/gallery_model");
        ci()->load->model("cms/picture_model");
        ci()->load->model("cms/producer_model");
        if (is_numeric($package_number));
        $item = ci()->api_model()->get_package_by_package_number($package_number);
        if (empty($item) && !is_numeric($package_number) && !empty($package_number)) {
            $item = ci()->package_model()->get_item_by_field("name", $package_number);
            # todo - ok?
        }
        $data = array();
        if ($item)
            ci()->api_model()->fill_packages(array($item), $data, true);
        return json_encode($data);
    }
}
<?php

/**
 * emarsys
 *
 * @see http://documentation.emarsys.com/resource/developers/getting-started/authentication/php-sample/
 * @see https://api.emarsys.net/api-demo/
 *
 */
class Emarsys
{
    private $_secret;
    private $_suiteApiUrl;
    private $_username;

    const EMAIL = 'email';
    const FIRSTNAME = 'firstname';
    const LASTNAME = 'lastname';
    const TITLE = 'title';

    const DELETE = 'DELETE';
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';

    const CREATING_A_CONTACT = 'creating_a_contact';
    const SIGN_OUT_FROM_EMAIL = 'sign_out_from_email';
    const SIGN_OUT_FROM_FORM = 'sign_out_from_form';
    const TRIGGERING_AN_EXTERNAL_EVENT_350 = 'triggering_an_external_event_350';
    const UPDATING_A_CONTACT = 'updating_a_contact';

    /**
     * SuiteApi constructor.
     */
    public function __construct()
    {
        $this->_secret = 'cgf4ACPZ6dI4WuBk0bpp';
        $this->_suiteApiUrl = 'https://api.emarsys.net/api/v2/';
        $this->_username = 'spar_mit_reisen002';
    }


    /**
     * @param $operation
     * @param $form_data
     * @return string
     */
    private function prepare_data($operation, $form_data)
    {
        switch ($operation) {

            case self::CREATING_A_CONTACT:
                // https://api.emarsys.net/api/v2/contact
                $arr = array(
                    1 => $form_data[self::FIRSTNAME],
                    2 => $form_data[self::LASTNAME],
                    3 => $form_data[self::EMAIL],
                    4494 => md5($form_data[self::EMAIL])
                );
                if (isset($form_data[self::TITLE])) {
                    switch ($form_data[self::TITLE]) {
                        case 'Herr':
                            $arr[46] = 1;
                            break;
                        case 'Frau':
                            $arr[46] = 2;
                            break;
                    }
                }

                $requestType = self::PUT;
                $endPoint = 'contact/?create_if_not_exists=1';
                break;

            case self::TRIGGERING_AN_EXTERNAL_EVENT_350:
                //https://api.emarsys.net/api/v2/event/350/trigger';
                $arr = array(
                    'key_id' => 3,
                    'external_id' => $form_data[self::EMAIL],
                );

                $requestType = self::POST;
                $endPoint = 'event/350/trigger';

                break;
            case self::UPDATING_A_CONTACT:
                //https://api.emarsys.net/api/v2/contact'
                $arr = array(
                    31 => 1,
                    4494 => $form_data['token'],
                    'key_id' => 4494,
                    6185 => 'Ja'
                );
                $requestType = self::PUT;
                $endPoint = 'contact';
                break;

            case self::SIGN_OUT_FROM_EMAIL:
                //https://api.emarsys.net/api/v2/contact'
                $arr = array(
                    31 => 2,
//                    4494 => $form_data['token'],
//                    'key_id' => 4494
                    3 => $form_data['email'],
                    'key_id' => 3
                );
                $requestType = self::PUT;
                $endPoint = 'contact';
                break;

            case self::SIGN_OUT_FROM_FORM:
                //https://api.emarsys.net/api/v2/contact'
                $arr = array(
                    31 => 2,
                    3 => $form_data[self::EMAIL],
                );
                $requestType = self::PUT;
                $endPoint = 'contact';
                break;

            default:
                die('Operation ist unbekannt');
                break;
        }
        return array($requestType, $endPoint, json_encode($arr));
    }

    /**
     * @param $operation
     * @param $form_data
     * @throws Exception
     */
    public function send($operation, $form_data)
    {
        list ($requestType, $endPoint, $requestBody) = self::prepare_data($operation, $form_data);

        if (!in_array($requestType, array('GET', 'POST', 'PUT', 'DELETE'))) {
            throw new Exception('Send first parameter must be "GET", "POST", "PUT" or "DELETE"');
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($requestType) {
            case 'GET':
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
            case 'DELETE':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
        }

        curl_setopt($ch, CURLOPT_HEADER, true);

        $requestUri = $this->_suiteApiUrl . $endPoint;
        curl_setopt($ch, CURLOPT_URL, $requestUri);

        /**
         * We add X-WSSE header for authentication.
         * Always use random 'nonce' for increased security.
         * timestamp: the current date/time in UTC format encoded as
         *   an ISO 8601 date string like '2010-12-31T15:30:59+00:00' or '2010-12-31T15:30:59Z'
         * passwordDigest looks sg like 'MDBhOTMwZGE0OTMxMjJlODAyNmE1ZWJhNTdmOTkxOWU4YzNjNWZkMw=='
         */

        $nonce = 'd36e316282959a9ed4c89851497a717f';
        $timestamp = gmdate("c");
        $passwordDigest = base64_encode(sha1($nonce . $timestamp . $this->_secret, false));

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-WSSE: UsernameToken ' . 'Username="' . $this->_username . '", ' . 'PasswordDigest="' . $passwordDigest . '", ' . 'Nonce="' . $nonce . '", ' . 'Created="' . $timestamp . '"',
            'Content-type: application/json;charset=\"utf-8\"',
        ));
        $response = curl_exec($ch);
        curl_close($ch);
//        return $response;
    }

}
<?php

class Packagestripe_lib
{
    public function __construct() {
        #deprecated! use ci()!
//        $this->CI = &get_instance();
    }

    /** @var  int $limit_num */
    public $limit_num;

    /**
     * @deprecated moved to api_lib - here is no cache used!
     * @param array $url_segments a/b/c/d
     * @param array $get_params key=>value pairs
     * @param array $post_params
     * @return string api-return-data
     */
    public function api_return($url_segments = [], array $get_params = [], array $post_params = []){
//        $limit_from = (get_instance()->input->post_get("limit_from")) ? (int) get_instance()->input->post_get("limit_from") : 0;
//        if ($limit_from) {
//            $params['limit_from'] = $limit_from;
//        }
        return ci()->curl_lib()->get_api_data($url_segments, [], $get_params, $post_params);
    }

    /**
     * @param object|Item  $item
     * @return string css-class for single element view
     */
    public function css_viewport($item){
        $css_class = '';
        if(isset($item['mobile']) && isset($item['mobile'])) {
            if(is_null($item['mobile'])) {
                $item['mobile'] = 1;
            }
            if(is_null($item['desktop'])) {
                $item['desktop'] = 1;
            }
        } else {
            $item['mobile'] = 1;
            $item['desktop'] = 1;
        }
        $css_class .= (!$item['mobile']) ? ' hidden-sm ': '';
        $css_class .= (!$item['desktop']) ? ' hidden-lg ': '';
        return $css_class;
    }
    
}
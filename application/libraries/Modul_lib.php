<?php

/**
 * User: pdahlenburg
 * Date: 10.04.17
 * Time: 11:10
 */

defined('BASEPATH') OR exit('No direct script access allowed');
class Modul_lib{

    public function getRegionModul($taOption){
        $countryLocations = "";
        $otherCountryList="";
        //  Countrys
            foreach ($taOption['countrys'] as $item) {
                $countries="";
                $classOut="disabledFalse";
                if(isset($taOption['regions2'])&&count($taOption['regions2'])>0) {
                    $count = "0";
                    foreach ($taOption['regions2'] as $counter) {
                        if ($item['properties']['id'] == $counter['id']) {
                            $count = $counter['count'];
                        }
                    }
                }
                else{
                    $count=$item['properties']['count'];
                    }
                $test4="";
                $checked1="";
                $checkboxDisabled="";


                if($count==='0'&&isset($_SESSION['search_Categorie'])&&$_SESSION['search_Categorie']!="reiseziele"){
                    $classOut = "disabledTrue";
                    $checkboxDisabled = "disabled='disabled'";

                }
                else{
                    $checkboxDisabled = "";
                }

                if(isset($_SESSION["selectedValue"]["region"])&&in_array($item['properties']['id'], $_SESSION["selectedValue"]["region"])) {
                    $checked1 = "checked='checked'";
                }


                $countries .= "<li class='country-list'><label><div class='row'><div class='col-sm col-checkbox'><input class='checbox_Region checbox_RegionItem $classOut' id='" . $item['properties']['id'] . "'type='checkbox' " . $checkboxDisabled . " data-title='" . $item['properties']["title"] . "' data-foldname='reiseziele' name='region[]'  value='" . $item['properties']['id'] . "|" . $item['properties']["title"] . "' " . $checked1 . "></div><div class='col-sm'><span class='country-title $classOut'>" . $item['properties']["title"] . " <small class='text-muted'>(" . $count . ")</small></span></div></div></label></li>";


                if(isset($item['locations'])){
                    $region=$this->getRegion($item['locations'],$taOption);
                }

                if($region!==""){
                    $countryLocations.= $countries.$region;

                }
                else{

                    $otherCountryList.= $countries;
                }

            }

        $modul = "<div class='modul-searchOption'>
                        <div class='wrapper_desktopsearch'>
                        <input id='inputcity' name='inputcity' data-foldname='inputCity' type='text' class='modulSearchbar' placeholder='Ort, Region, Land' />
                        <button class='inputCityButtton' " . $taOption['buttonInputCity'] . " > <i id='filtersubmit' class='fa fa-search'></i></button>
                        </div>
                        <div class='subTitleBox'>
                        <div class='displaySelectedOptionsCheckbox'></div>
                    <div class='subTitle subTitleDesktopDetailsearch'>Beliebteste Reiseziele</div>    
                </div></div> 
                
                <div class='columns'>
                <ul class='no-bullets no-margin-top' id='kategorieInhalt'>
                $countryLocations
                <li class='country-list'><span class='country-title'>Oder nach:</span></li>   
                <div class='other_countries'>
                 $otherCountryList
                 </div>
                </ul>
               </div>
          
          <script>
        $( document ).ready(function() {
            
          
          if($('form')[0].name==='submitform'){
                   sessionStorage.clear();      
          }
         
               
            
                  $('input[type=checkbox]').each(function () {
                 if(this.checked) {
                   showSubmitedSelectItem($(this));                   
                   }
              
            });
            
            $('.checbox_Region').change(function() {
 
                        changeCheckboxDetailsearch($(this));
                
                });
            
            $('.wrapper_desktopsearch #inputcity').autocomplete({
    source: scope.site_url + 'Search/AutoFill',
    data:{value:$('.wrapper_desktopsearch #inputcity').val()},
    delay: 500,
    minLength: 3,
    open: function() {
        $('ul.ui-menu').width($('.wrapper_desktopsearch #inputcity').width());
        $('ul.ui-menu').width($('.wrapper_desktopsearch #inputcity').width());
    },
    select: function(event, ui) {
        if(ui.item){
            $('.wrapper_desktopsearch #inputcity').val(ui.item.value);
        }
        $('.wrapper_desktopsearch #inputcity').submit();
    }
});
          
            
            // Diese Funktion wurde in modal.js ausgelagert
            $('.ui-dialog-titlebar-close').click(function(){
                close();
            });

          


        });
        </script> ";
        return $modul;

    }



    public function getRegion($taRegionArry,$taOptions){

            $regionTable = "";
            $checkboxDisabled = "";

            foreach ($taRegionArry as $city) {

                $count2 = "0";
                if (isset($taOptions['regions2']) && count($taOptions['regions2']) > 0) {


                    foreach ($taOptions['regions2'] as $counter) {
                        if ($city['location_id'] == $counter['id']) {
                            $count2 = $counter['count'];
                        }

                    }
                } else {
                    $count2 = $city['count'];
                }

                $class = "disabledFalse";
                if ($count2 == 0) {
                    $class = "disabledTrue";
                    $checkboxDisabled = "disabled='disabled'";
                } else {
                    $class = "";
                    $checkboxDisabled = "";
                }
                $checked = "";


                if (isset($_SESSION["selectedValue"]["region"]) && in_array($city['location_id'], $_SESSION["selectedValue"]["region"])) {
                    $checked = "checked='checked'";

                }

                $regionTable .= "<li><label><div class='row'><div class='col-sm col-checkbox'><input class='checbox_Region checbox_RegionItem $class' id='" . $city['location_id'] . "'type='checkbox' " . $checkboxDisabled . "data-title='" . $city["title"] . "' data-foldname='reiseziele' name='region[]'  value='" . $city['location_id'] . "|" . $city["title"] . "' " . $checked . "></div><div class='col-sm'><span class='region-title $class'>" . $city["title"] . " <small class='text-muted'>(" . $count2 . ")</small></span></div></div></label></li>";

            }
        return $regionTable;
    }

    public function getTopicModul($taOption){

        $topic="";
        $extraTable="";

        foreach ($taOption['categories'] as $item) {
            $class = "disabledFalse";
            $checkBoxAttribut="";
            if ($item['count'] == 0) {
                $class = "disabledTrue";
                $checkBoxAttribut='disabled="disabled"';
            }
            $checked = "";
            if(isset($_SESSION["selectedValue"]["reisethemen"])&&in_array($item['id'], $_SESSION["selectedValue"]["reisethemen"])) {
                    $checked = "checked='checked'";
                }
            $topic .= "<li><label><div class='row'><div class='col-sm col-checkbox'><input class='checbox_Country-row checbox_reiseart $class ' data-title='" . $item['name'] . "' data-foldname='reisethemen' $checkBoxAttribut type='checkbox' name='topics[]' id='" . $item['id'] . "' value='". $item['id'] ."|".$item['name']."' ".$checked."></div><div class='col-sm'><span class='checbox_reiseart $class'>" . $item['name'] . " <small class='text-muted'>(" . $item['count'] . ")</small></span></div></div></label></li>";
        }

        $extraTable= "<div class='subTitleBox'>
                    <div class='displaySelectedOptionsCheckbox'></div>
                </div> <div id='kategorieInhalt' class='kategorieInhalt table-flex'>
                 <div class='columns'>
                <ul class='no-bullets no-margin-top' id='kategorieInhalt'>
                $topic
               
                </ul>
               </div>
           </div>       
          
          
          
          <script>
        $( document ).ready(function() {
           
            
      


            $('input[type=checkbox]').each(function () {
                 if(this.checked) {
                   showSubmitedSelectItem($(this));
                   
                   }
                 
            });
            $('.checbox_reiseart').change(function() {
              
                changeCheckboxDetailsearch($(this));
                 
                });
          
            $('.ui-dialog-titlebar-close').click(function(){
                close();
            });
        });
       
        </script>  ";
        return $extraTable;
    }
    public function getExtraModul($taOption){
        $extras="";
        $extraTable="";
        $checkBoxAttribut="";
        foreach ($taOption['extras'] as $item) {
            $countryLocations = "";

            $extraList="<div class='extraList'><div class='subtitle extrasdesktopSearchTitle'>".$item['name']."</div> ";
            foreach ($item['values'] as $key => $item2) {
                $name = ci()->search_lib->translateOptions($key);
                $checkBoxAttribut="";
                $class = "disabledFalse";
                if ($item2 == 0) {
                    $class = "disabledTrue";
                    $checkBoxAttribut='disabled="disabled"';
                }

                $checked="";
                if(isset($_SESSION["selectedValue"]["extrawuensche"])&&in_array($key, $_SESSION["selectedValue"]["extrawuensche"])){
                    $checked = "checked='checked'";
                }
                $countryLocations .= "<li><label><div class='row'><div class='col-sm col-checkbox'><input class='checbox_Region-row checbox_extras $class' id='". $key ."'data-title='" .$name. "' $checkBoxAttribut data-foldname='extrawuensche' value='" . $key . "' data-inputname='" . $key . "'type='checkbox' name='extrawuensche[]' ".$checked."></div><div class='col-sm'><span class='checbox_extras $class'>" . $name . " <small class='text-muted'>(" . $item2 . ")</small></span></div></div></label></li>";
                }

            $extraList.=$countryLocations."</div>";
            $extraTable.=$extraList;

        }
        $extras .="<div class='modul-searchOption'>
                    <div class='displaySelectedOptionsCheckbox'></div>
                </div> <div id='kategorieInhalt' class='kategorieInhalt table-flex-extras'>
                                 <div class='columns'>
                <ul class='no-bullets no-margin-top' id='kategorieInhalt'>
                $extraTable
                 </ul>
               </div>
          </div>
             <script>
        $( document ).ready(function() {
           
            
               $('input[type=checkbox]').each(function () {
                 if(this.checked) {
                   showSubmitedSelectItem($(this));
                   
                   }
            });

            $('.checbox_extras').change(function() {
              
                changeCheckboxDetailsearch($(this));
                 
                });
          
            $('.ui-dialog-titlebar-close').click(function(){
                close();

//                if ($('input[type=checkbox]').is(':checked')&&$('form')[0].name==='submitform') {
//                    
//                    sessionStorage.clear();
//                
//                    $('#form1').submit();
//                }
//                else{
//                     close();
//                }
//                if ($('input[type=checkbox]').is(':checked')&&$('form')[0].name==='ajaxform') {
//                  
//
//                }
//                else{
//                     close();
//                }
            });
        });
       
        </script> ";
        return $extras;
    }
    public function getDateModul($taOptions){
        $date=[];


        if(isset($taOptions['date'])){
            $date=json_encode($taOptions['date']);
        }
        else{
            $date=json_encode($date);
        }
        $inputfields="";
        for ($i=2;$i<=14;$i++){

         $inputfields.="<div class='col-sm-4'><div class='radio'>
                      <label class='modReisTerAnzUeb'><input type='checkbox' class='anzTageInput' data-foldname='inputTravelDate' name='anzTage[]' value='".$i."' >".$i." Übernachtungen </label>
                    </div></div>";
        }
        $inputfields.="<div class='col-sm-8'><div class='radio'>
                      <label class='modReisTerAnzUeb'><input type='checkbox' class='anzTageInput' data-foldname='inputTravelDate' name='anzTage[]' value='15+' >mehr als 15 Übernachtungen </label>
                    </div></div>";

        $datePicker="
        <div class='kategorieInhalt' id='kategorieInhalt'>
            <div class='row'>
                <div class='col-sm-6'>
                     <div class='row'>
                         <div class='col-sm-6'><div class='datepickerTitle'> Anreise</div></div>
                           <div class='col-sm-6'>
                                <input type='text'  data-foldname='inputTravelDate' class='datepickerhere' name='anreiseDatum' readonly id='checkInDate' data-inputname='checkInDate'>
                            </div>
                     </div>
                        <div class='desktop_search_picker' id='datepicker'></div>
                    </div>
                <div class='col-sm-6'>
                   <div class='row'>
                         <div class='col-sm-6'><div class='datepickerTitle'> Abreise</div></div>
                           <div class='col-sm-6'>
                            <input type='text' data-foldname='inputTravelDate' class='datepickerhere' name='abreiseDatum' id='checkOutDate' data-language='de' readonly data-inputname='checkOutDate'  onchange='javascript:checkDate();return false;' />
                           </div>
                           
                    </div>
                    <div class='desktop_search_picker' id='datepicker2'></div>
                </div>
            </div>
            <!--div class='modReisTeranzZuTageContainer' style='display: none'>
             <div class='col-sm-12'>
                   <div class='col-sm-10 modReisTerzuTit'><div class='datepickerTitle'>Ihre gewünschte Reisedauer</div></div>
                        <div class='radio modReisTerRadioTitel'>
                        <input type='radio' class='anzTageInputAllDays' id='allDays' name='anzTage[]' value='' checked='checked'><label class='radioDetailReisetermin' >Ich möchte so lange verreisen wie oben ausgewählt:<br/><div class='inputDays'></div> </label>
                    </div>
                     <div class='modReisTerRadioTitel'>
                    <input type='radio' class='anzTageInputAllDays' id='selectedDays' name='anzTage' value='1' ><label class='radioDetailReisetermin'>Innerhalb des oben angegebenen Zeitraums
                        möchte ich kürzer verreisen, <br/>und zwar so lange:
                        (Sie können hier auch mehrere Haken setzen)</label>
                    </div>
             </div>
             
                 <!--div class='row modReisTerRadioTitelSelect'>
                    ".$inputfields."</div>
             </div-->
        
        <script>
        $( document ).ready(function() {
           
                 $('.ui-dialog-titlebar-close').click(function(){
                     close();
            });
                 
                  $('.anzTageInputAllDays').change(function() {
                      
                      
                     
                      
            $('.anzTageInputAllDays').not(this).prop('checked', false);
       
            });
            
          $( function() {
                $( '#datepicker').datepicker({
                    numberOfMonths: 1,
                    language: 'de',
                    monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni',
                    'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                     monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun',
                        'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
                     dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                     dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                    dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                    dateFormat: 'dd.mm.yy',
                    prevText: '<i class=\"fa fa-chevron-left\"></i><i class=\"fa fa-chevron-left\"></i><i class=\"fa fa-chevron-left\"></i>',
                    nextText: '<i class=\"fa fa-chevron-right\"></i><i class=\"fa fa-chevron-right\"></i><i class=\"fa fa-chevron-right\"></i>',
                    beforeShowDay: SetDayStyle,
                       altField: '#checkInDate',
                    firstDay: 1
                });
                
            });
            $( function() {
                $( '#datepicker2').datepicker({
                    numberOfMonths: 1,
                    language: 'de',
                    monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni',
                    'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                     monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun',
                        'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
                     dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                     dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                    dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                    minDate: '',
                    dateFormat: 'dd.mm.yy',
                    prevText: '<i class=\"fa fa-chevron-left\"></i><i class=\"fa fa-chevron-left\"></i><i class=\"fa fa-chevron-left\"></i>',
                    nextText: '<i class=\"fa fa-chevron-right\"></i><i class=\"fa fa-chevron-right\"></i><i class=\"fa fa-chevron-right\"></i>',
                    beforeShowDay: SetDayStyle,
                    altField: '',
                    firstDay: 1
                });
                
            });
            
              $(function () {
              
                  
            $('#datepicker2 .ui-datepicker-calendar tbody tr td').removeClass('ui-datepicker-current-day');
              $('#datepicker2 .ui-datepicker-calendar tbody tr td a').removeClass('ui-state-active');
            
                  
               
                  
                  $('#datepicker').datepicker().change(function () {
                  
              var date =$('#datepicker').datepicker('getDate' );
                  date.setMonth(date.getMonth());
              $('#datepicker2').datepicker('setDate',date);
              $('#datepicker2 .ui-datepicker-calendar tbody tr td').removeClass('ui-datepicker-current-day');
              $('#datepicker2 .ui-datepicker-calendar tbody tr td a').removeClass('ui-state-active');
            
        
              changeCloseButton();
                   
                  
                  
                  });
                  
                $('#datepicker2').datepicker().change(function () {
                                
                   
                    $('#checkOutDate').val($('#datepicker2').datepicker({ dateFormat: 'dd.mm.yy' }).val());
                   
                    changeCloseButton();
                                    
                });
                
                function changeCloseButton() {
                  var startDate = new Date($('#datepicker').datepicker('getDate' ));
                    var endDate = new Date($('#datepicker2').datepicker('getDate' ));
                    
                    if(endDate>startDate){
                    
                    //nextDay.setDate(nextDay.getDate() + 2);
                    //$('#datepicker2').datepicker('option', 'minDate', nextDay);
                    $( '.ui-dialog-titlebar-close' ).addClass('closeButtonSelected' );
                    $('input[name=changed]').val('1');
                     $('#allDays').prop('checked', true);
                     $('#selectedDays').prop('checked', false);
                    
                    changeAnzDay();
                     }
                     else{
                          $( '.ui-dialog-titlebar-close' ).removeClass('closeButtonSelected');                     
                            $('input[name=changed]').attr('value', ''); 
                    
                     }
                }
                
                
                 $('#datepicker2').datepicker().change(function () {
                   $('#allDays').prop('checked', true);
                   $('#selectedDays').prop('checked', false);
                    changeAnzDay();
                    
                });
                 $('.anzTageInputAllDays').change(function(){
                      if($('#allDays').is(':checked')){
                           $('.anzTageInput').prop('checked', false); 
                          $('.anzTageInput').attr('disabled',true);
                          $('.modReisTerAnzUeb').addClass('disabledTrue');
                      }
                      else{
                           $('.anzTageInput').removeAttr('disabled');
                           $('.modReisTerAnzUeb').removeClass('disabledTrue');
                      }
                 });
            });
            
             function changeAnzDay(){
                 $('.modReisTeranzZuTageContainer').css('display','block');
                   $('.anzTageInput').prop('checked', false); 
                   $('.anzTageInput').attr('disabled',true);
                   $('.modReisTerAnzUeb').addClass('disabledTrue');
                 var start= $('#datepicker').datepicker('getDate');
                var end= $('#datepicker2').datepicker('getDate');
                
                var stDate=$('#datepicker').val().split('.',2).toString();
                 stDate=stDate.replace(',','.');
                var datumText=stDate+'-'+$('#datepicker2').val();
                 
                 days = (end- start) / (1000 * 60 * 60 * 24);
                 $('.inputDays').html('<b>'+datumText+'</b> = '+days+' Übernachtungen');
                 $('#allDays').val(days);
             }
               
            function SetDayStyle(date) {
             var today = new Date(), maxDate;
            today.setHours(0,0,0,0);
            maxDate = new Date().setDate(today.getDate() + 17);
            if (date > today ) {
                return [true, 'greendark'];
            }
            else if (date<today){
                return [false, 'blue'];
            }
            return [true, ''];
            }
           
             $( function() {
                var dateArray=".$date.";
               
                if(dateArray.length>0){
                  ranges=[];
                    
                   
                    $.each(dateArray, function( index, value ){
                    ranges.push({
                        start:new Date(value.von),
                        end:  new Date(value.bis)  
                    });
                  });
              
                $('.desktop_search_picker').datepicker('option', 'beforeShowDay', function (date) {
                    var today = new Date(), maxDate;
                  
                    today.setHours(0,0,0,0);
                    maxDate = new Date().setDate(today.getDate() + 17);
                    if (date <today ) {
                        return [false, 'detailsearch_pastDates'];
                    }
                    for(var i=0; i<ranges.length; i++) {
                        
                        if(date >= ranges[i].start && date <= ranges[i].end) return [true, 'detailsearch_avaible'];
                    }
                    return [false, 'detailsearch_notavaible'];
                });
                $('#ui-datepicker-div').css('display','none');
            }
             });
           
        });
        </script> ";
        return $datePicker;
    }
    public function getDistanceModul($taOptions){
        $startPLZ="";
        $maxRange="";
        $startPLZ= @$_COOKIE['departure_plz'];
            $distance="
        <div id='kategorieInhalt'>
            <div class='row'>
                <div class='col-sm-6 inputDetailsearchEnt'>
                    <h3 class='subtitle txtDetailSearchEnt'>Hier geht die Reise los</h3>
                </div>
                <div class='col-sm-6 inputDetailsearchEnt'>
                    <div class='input-group'>
                    <div class='wrapper_desktopsearch'>
                        <input type='text'  class='startPLZ PLZInputDesktop' placeholder='PLZ' maxlength='5' name='startPlz' data-foldname='startPlz' id='startPlz' value='' data-inputname='startPlz' '>
                    </div>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col-sm-6 inputDetailsearchEnt'>
                    <h3 class='subtitle txtDetailSearchEnt'>Maximale Entfernung <span class='entLuftLine'>(Luftlinie)</span></h3>
                </div>
                <div class='col-sm-6 inputDetailsearchEnt'>
                    <div class='input-group'>
                    <div class='wrapper_desktopsearch'>
                            <input type='text' class='maxRangeInputDesktop' placeholder='km' maxlength='4' name='maxRange' data-foldname='maxRange' id='maxRange' data-foldname='maxRange' value='".$maxRange."' data-inputname='maxRange' onchange='' />
                            <button class='inputCityButtton' onclick='writefLatLon();' style='height: 30px;' ".$taOptions['buttonInputCity']."> <i id='filtersubmit' class='fa fa-search'></i></button>
                        </div>
                    
                    </div>
                </div>
            </div>
            <input type='hidden' data-foldname='' name='lon' class='modul-lon'>
            <input type='hidden' data-foldname='' name='lat' class='modul-lat'>
        </div>  
        
        
        <script>
        $( document ).ready(function() {
            
           $('.PLZInputDesktop').val(getCookie('departure_plz'));
         
            $('.maxRangeInputDesktop').keyup(function()  {
                     
                        changeCloseButtonRange($(this));
                
             });
            
             $('.PLZInputDesktop').keyup(function()  {
                           
                        changeCloseButtonRange($(this));
                
             });
          
            $('.ui-dialog-titlebar-close').click(function(){
                close();
               
            });
        });
        </script> ";
            return $distance;
        }
    private function getLocationsData(){
    }
    public function noFoundDisplay(){


        $noFound='<div class="box"><figure class="hero-image hidden-sm hidden-sm hidden-mds hidden-mdl">
                    <img src="' . site_url("assets/img/suche/suche-headerbild.jpg") . '" alt="Keine Ergebnisse gefunden">
                    <div class="header-info">
                        <strong>Stefanie Natschke</strong><br>
                        Reiseberaterin
                    </div>
                    <div class="label-wrap top-left">
                        <div class="label-title">
                            <h2>Schade, kein Treffer!</h2>
                        </div>
                        <div class="label-subtitle middle-title">
                            <h2 class="long-text">Zu Ihren Suchkriterien<br>wurde nichts gefunden.<span class="red">
                               Aber ich helfe Ihnen gerne weiter.
                               </span>
                                <div class="phone">
                                   <span class="times">
                               Telefonische Beratung </span>
                                    <span class="number">
                                       07621 - 91 40 111 <span>(täglich 7.00 - 21.00 Uhr)</span>
                                   </span>
                                </div>
                            </h2>
                        </div>
                    </div>
                </figure></div>';
      return $noFound;
    }
}
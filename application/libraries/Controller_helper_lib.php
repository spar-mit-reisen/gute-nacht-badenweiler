<?php

MY_Controller::get_instance()->load->library("super_lib");

/**
 * Description of Controller_helper_lib
 *
 * @author sebra
 */
class Controller_helper_lib extends Super_lib
{
	/**
     * reads foreign_keys from input or parameter and sets it to the item
	 * @param Item $item
	 * @param MY_Controller $CI
	 * @param array|null $foreign_keys array of foreign_keys, if null, gets them from imput
	 */
	public function assign_foreign_keys_to_item(&$item, &$CI, $foreign_keys = null){
		if (is_null($foreign_keys))
			$foreign_keys = $CI->input->post_get("foreign_keys");
		if (is_array($foreign_keys)) {
			if (!is_array($item->foreign_keys))
				$item->foreign_keys = array();
			foreach ($foreign_keys as $k => $v) {
				$item->foreign_keys[$k] = $v;
			}
		}
	}
	/**
	 *
	 * @param array $output
	 * @param array $data
	 * @param int $object1_id
	 * @param string $object1_type object-type of object the id is for
	 * @param string $object2_type object-type of objects you want to get
	 * @throws Exception
	 */
	public static function get_linked_list(&$output, &$data, $object1_id, $object1_type, $object2_type) {
		if ($object1_type == $object2_type)
			throw new Exception("Objekte eines Typs können nicht mit Objekten des selben Typs verknüpft werden.");
		$CI = MY_Controller::get_instance();
        /** @var MY_Model $model */
		$model = $CI->{$object2_type . "_model"};
		if ($object2_type == "picture"){
		    $picture_model = $model;
		    $CI->load->model("cms/knot_model");
		    $knot_model = $CI->knot_model;
		    if ($object1_type == "gallery") {
                $items = ci()->gallery_model()->get_pictures_by_gallery($object1_id);
            } else
		        $items = $picture_model->get_items_by_knot($object1_id, $object1_type, $object2_type, null, $knot_model);

            # load also old_picture ..
		    $CI->load->model("cms/old_picture_model");
            $old_picture_model = $CI->old_picture_model;
//            #$items_old = $old_picture_model->get_items_by_knot($object1_id, $object1_type, "old_".$object2_type, null, $knot_model);
//            var_dump($items_old);
//            #$items = array_merge($items, $items_old);
            # .. load also old_picture

            $data['box_index']['list_row_view'] = "cms/picture/list_row_view";
            $data['box_index']['list_fields'] = $model->link_list_fields;
            $data['box_index']['list_fields_2D'] = $model->get_fields_2D($model->link_list_fields);
            $data['box_index']['sortable'] = true;
            $data['box_index']['sort_url'] = site_url("cms/knot/sort/$object1_type/$object1_id/$object2_type");
            $data['box_index']['list_action_view'] = "cms/$object2_type/list_actions_view";
        } elseif ( $object2_type == "gallery" ){
            $items = $model->get_items_by_knot($object1_id, $object1_type, $object2_type);
            $data['box_index']['sortable'] = true;
            $data['box_index']['sort_url'] = site_url("cms/knot/sort/$object1_type/$object1_id/$object2_type");
        } else {
            if (method_exists($model, "list_fields_2D"))
                $data['box_index']['list_fields_2D'] = $model->list_fields_2D($object1_type);
            $data['box_index']['list_fields'] = array_keys($data['box_index']['list_fields_2D']);
//            ci()->dump($data['box_index']['list_fields_2D']);
            $items = $model->get_items_by_knot($object1_id, $object1_type, $object2_type);
        }
//		var_dump($items);echo "<br>\n";
        $model->get_related($object1_type, $items);
        #
        if ($object1_type == "topic" && $object2_type == "content"){
            $topic_id = $object1_id;
            foreach ($items as $item) {
//                $item->topic_knot_item = $model->db()->where([])->get('object_object_knots')->result_object();
                $item->topic_knot_item = $model->get_item_by_knot_array(['topic'=> $topic_id, 'content'=> $item->id], false);
                if ($item->id == 1876){
//                    var_dump($item);die;
//                    ci()->dump_query($model->last_query());
//                    ci()->dump($item);
                }

            }
        }
//        ci()->dump($items);
		if ( 1 || $items ) {
			$model->get_data_for_list($items, 0, $data);
			$data['box_index']['create_params'] = "?{$object1_type}_id=$object1_id&foreign_keys[$object1_type]=$object1_id";
			$data['box_index']['modelname'] = $model->table;
//			$data['box_index']['foreign_keys']["{$object_type1}_id"] = $id1;
			$data['box_index']['foreign_keys']["{$object1_type}"] = $object1_id;
			$output[] = $CI->get_view("cms/list_view", $data);
		}
	}

	/**
	 *
	 * @param array $output
	 * @param array $data
	 * @param int $id1
	 * @param string $object_type1 object-type of object the id is for
	 * @param string $object_type2 object-type of objects you want to get
	 * @throws Exception
	 */
	public function get_dogtag_list(array &$output, array &$data, $id1, string $object_type1, string $object_type2 = "dogtag") {
//		/** @var $model MY_Model */
		/** @var $dogtag_knot_model Dogtag_knot_model */
        $CI = MY_Controller::get_instance();
        $model = $CI->any_model($object_type2);

        $items = $this->get_dogtag_list_items($id1, $object_type1, $object_type2);
		if ( 1 || $items ) {
            # Standardwerte für View laden
            $model->get_data_for_list($items, 0, $data);
            $data['box_index']['create_params'] = "?{$object_type1}_id=$id1&foreign_keys[$object_type1]=$id1";
            $data['box_index']['modelname'] = $model->table;
            # Tabellen-/Listen-Überschrift setzen
            if ($object_type1 == "hotel")
                $data['box_index']['title'] = "Location (" . $model->items_label . ")";
            else
                $data['box_index']['title'] = $model->items_label;
            # Fremdschlüssel übergeben
            $data['box_index']['foreign_keys']["{$object_type1}"] = $id1;

            # Spalten-Felder an View übergeben
            if ($object_type1 == "topic" && $object_type2 == "dogtag") {
                $model = ci()->dogtag_model();
                $data['box_index']['list_fields_2D'] = $model->topic_link_list_fields_2D;
            } elseif ($object_type2 == "topic" && $object_type1 == "dogtag") {
                $model = ci()->topic_model();
                $data['box_index']['list_fields_2D'] = $model->list_fields_2D($object_type1);
            } elseif (isset($model->link_list_fields_2D))
				$data['box_index']['list_fields_2D'] = $model->link_list_fields_2D;
			elseif (isset($model->link_list_fields))
				$data['box_index']['list_fields'] = $model->link_list_fields;
			else
				$data['box_index']['list_fields'] = $model->list_fields;
            # Bei Paketen und Hotels kein required/notwendig anzeigen, braucht man nur bei Themen-/Kategorie-Seiten
            if (
                ($object_type2 == "dogtag" && in_array($object_type1, ["package", "hotel"]))
                || ($object_type1 == "dogtag" && in_array($object_type2, ["package", "hotel"]))
            ){
                unset($data['box_index']['list_fields_2D']['required']);
                unset($data['box_index']['list_fields']['required']);
            }

			# View laden
			if (in_array($object_type2, array("dogtag","locationtag"))){
				$data['box_index']['list_row_view'] = "cms/dogtag/list_row_view";
				$data['box_index']['action_view'] = "cms/dogtag/link_list_row_actions_view";
				$data['box_index']['list_linking_view'] = "cms/dogtag/list_linking_view";
				$data['box_index']['hide_actions'] = true;
				$data['box_index']['show_empty_table'] = true;
				$output[] = $CI->get_view("cms/dogtag/link_list_view", $data);
			}
			else {
				$output[] = $CI->get_view("cms/list_view", $data);
			}
		}
	}

    /**
     * @param int $id1
     * @param string $object_type1
     * @param string $object_type2
     * @return array of object-items
     * @throws Exception
     */
    public function get_dogtag_list_items($id1, string $object_type1, string $object_type2 = "dogtag"): array {
        /** @var Dogtag_knot_model $dogtag_knot_model */

        if ($object_type1 == $object_type2)
            throw new Exception("Objekte eines Typs können nicht mit Objekten des selben Typs verknüpft werden.");
        $CI = MY_Controller::get_instance();
        if ($object_type2 == "dogtag")
            $model = $CI->dogtag_model();
        else
            $model = $CI->any_model($object_type2);
        if (in_array($object_type2, array("dogtag", "locationtag"))) {
            $CI->load->model("cms/" . "dogtag" . "_knot_model");
            $dogtag_knot_model = $CI->{"dogtag" . "_knot_model"};
        }
        if (in_array($object_type1, array("dogtag", "locationtag"))) {
            $CI->load->model("cms/" . $object_type1 . "_knot_model");
            $dogtag_knot_model = $CI->{$object_type1 . "_knot_model"};
        }
        // TODO - move this to $dogtag_model->get_items_by_knot
        if ($object_type1 == "package" && $object_type2 == "dogtag") {
            $package_id = $id1;
            if (0) {
                $hotel_items = $CI->hotel_model()->get_items_by_knot($package_id, "package");
                $location_dogtag_items = [];
                foreach ($hotel_items as $hotel_item) {
                    $location_dogtag_items_for_hotel = $CI->dogtag_model()->get_items_by_knot($hotel_item->id, "hotel", "dogtag", null, $dogtag_knot_model);
                    $location_dogtag_items = array_merge($location_dogtag_items, $location_dogtag_items_for_hotel);
                }
                foreach ($location_dogtag_items as $location_dogtag_item) {
                    $location_dogtag_item->__READ_ONLY = true;
                }
                $items = $CI->dogtag_model()->get_items_by_knot($id1, $object_type1, $object_type2, null, $dogtag_knot_model);
                if (!empty($location_dogtag_items))
                    $items = array_merge($items, $location_dogtag_items);
            } else {
                $items = $CI->dogtag_model()->get_items_by_package($package_id, "package+hotel");
            }
        } else { # object_type1 topic/hotel/etc...
            $items = $model->get_items_by_knot($id1, $object_type1, $object_type2, null, $dogtag_knot_model);
        }

//		var_dump($items);echo "<br>\n";

        $model->get_related($object_type1, $items);
        return $items;
    }

    /**
     * @param array $data
     * @param int $object1_id
     * @param string $object1_type
     * @param string $object2_type works only for package!!!
     * @param int $limit_num
     * @param int $limit_from
     * @return string for output
     */
    public function get_item_list_by_dogtag_score(array &$data, $object1_id, string $object1_type, string $object2_type, $limit_num = 0, $limit_from = 0): string {
	    $CI = MY_Controller::get_instance();
        $model = $package_model = $CI->package_model();

        if ($object1_type == "topic"){
            $data['box_index']['box_index_action'] = site_url("cms/topic/list_packages/$object1_id");
        }
        elseif ($object1_type == "package"){
            $data['box_index']['box_index_action'] = site_url("cms/$object1_type/list_packages_by_score/$object1_id");
        }

        $items = $this->get_items_by_dogtag_score($object1_id, $object1_type, $object2_type, $limit_num, $limit_from);

        if ( 1 || $items ) {
            $model->list_fields = array("id", "name", "package_number", "score", "action");
            $model->list_fields_2D = $model->get_fields_2D($model->list_fields);
            $model->get_data_for_list($items, 0, $data);
            $data['box_index']['title'] = "Liste verwandte " . $model->item_label ." (über Hundemarke/Scoring verknüpft)";
            $data['box_index']['create_params'] = "?{$object1_type}_id=$object1_id&foreign_keys[$object1_type]=$object1_id";
            $data['box_index']['modelname'] = $model->table;
            $data['box_index']['hide_actions'] = true;
            $data['box_index']['hide_link_button'] = true;
            if ($object1_type != $object1_type)#foreign_keys are without a direction! same objects need a relation type
                $data['box_index']['foreign_keys']["{$object1_type}"] = $object1_id;

            /* bewirkt refresh der Pakete beim Speichern der Scores */
//            $output[] = "<script>$(function(){\$('.box_dogtag_index.controller_topic .save.btn').click(function(){ajax_submit('.box_package_index.controller_topic');});})</script>";
//            $output[] = "";# why?
            $output[] = $CI->get_view("cms/list_view", $data);
        }

        return implode("", $output);
    }

    /**
     * return array of items
     * @param int $object1_id
     * @param string $object1_type
     * @param string $object2_type
     * @param int $limit_num
     * @param int $limit_from
     * @return array of Item objects
     */
    public function get_items_by_dogtag_score($object1_id, string $object1_type, string $object2_type, $limit_num = 0, $limit_from = 0): array {
        $CI = MY_Controller::get_instance();
        $dogtag_knot_model = $CI->dogtag_knot_model();
        $dogtag_model = $CI->dogtag_model();
        $model = $package_model = $CI->package_model();

//        $object1_type = "topic";
//        $object1_id = "5";

        if ($object1_type == "topic"){
            $data['box_index']['box_index_action'] = site_url("cms/topic/list_packages/$object1_id");
            $package_items = $package_model->get_package_items_sorted_by_score_to_object($object1_type, $object1_id, $limit_num, $limit_from);
        }
        elseif ($object1_type == "package"){
            $data['box_index']['box_index_action'] = site_url("cms/$object1_type/list_packages_by_score/$object1_id");
            $package_items = $package_model->get_package_items_sorted_by_score_to_object($object1_type, $object1_id, $limit_num, $limit_from);
        }
        else
            $package_items = $package_model->get_package_items_sorted_by_score_to_object($object1_type, $object1_id, $limit_num, $limit_from);

        $items = $package_items;
        return $items;
    }
    /**
     * return array of items
     * @param int $object1_id
     * @param string $object1_type
     * @param string $expected_object2_type
     * @param array $params limit_num, limit_from, return_type (=items|count)
     * @return array of Item objects
     */
    public function get_items_by_dogtag_score2($object1_id, string $object1_type, string $expected_object2_type = "package", array $params = []): array {
        if ($expected_object2_type == "package") {
            $package_model = ci()->package_model();
            if ($object1_type == "topic") {
                $data['box_index']['box_index_action'] = site_url("cms/topic/list_packages/$object1_id");
                $package_items = $package_model->get_package_items_sorted_by_score_to_object2($object1_type, $object1_id, $params);
            } elseif ($object1_type == "package") {
                $data['box_index']['box_index_action'] = site_url("cms/$object1_type/list_packages_by_score/$object1_id");
                $package_items = $package_model->get_package_items_sorted_by_score_to_object2($object1_type, $object1_id, $params);
            } else
                $package_items = $package_model->get_package_items_sorted_by_score_to_object2($object1_type, $object1_id, $params);

            $items = $package_items;
        }
        return $items;
    }

	/**
	 * @param $dogtag_model Dogtag_model
	 * @param $data array
	 * @param $CI MY_Controller
	 * @param $dogtag_item stdClass
	 * @param $object_type string
	 * @param $object_id int
	 * @return string html for output
	 */
	public function dogtag_ajax_replace(&$dogtag_model, &$data, &$CI, &$dogtag_item, &$object_type, &$object_id){
		$dogtag_items = array($dogtag_item);
		$dogtag_model->load($object_type,$dogtag_items);
		$dogtag_data['item'] = $dogtag_item;
		$dogtag_data['list_fields'] = $dogtag_model->list_fields;
		$dogtag_data['action_view'] = "cms/dogtag/actions_view";
		$dogtag_data['box_index'] = array("modelname" => "dogtag", "controllername" => "cms/dogtag", "item" => $dogtag_item);
		$dogtag_data['box_index']['foreign_keys'][$object_type] = $object_id;
		return $CI->get_view("cms/dogtag/list_row_view", $dogtag_data);
	}
	/**
     * Returns tr-html-string for a Dogtag-Row
	 * @param Dogtag_model  $dogtag_model
	 * @param array         $data array
	 * @param MY_Controller $CI
	 * @param object        $dogtag_knot_item
	 * @param string        $object_type Object-Type of Object you want to give dogtags e.g. package, hotel
	 * @param int           $object_id
	 * @return string       tr-html for output from view
	 */
	public function dogtag_linked_row(&$dogtag_model, &$data, &$CI, &$dogtag_knot_item, &$object_type, &$object_id){
//        $dogtag_knot_model->load($object_type,$dogtag_items);
		$dogtag_item = $dogtag_knot_item;
//		$dogtag_item = $dogtag_model->get_item_by_dogtag_knot_item($dogtag_knot_item);

		$dogtag_item->{$object_type}[$object_id] = $object_id;
		$items = $dogtag_knot_items = array($dogtag_knot_item);
		$dogtag_model->get_related($object_type, $items);

		$dogtag_knot_data['item'] = $dogtag_item;
		$dogtag_knot_data['box_index'] = array("modelname" => "dogtag", "controllername" => "cms/dogtag", "item" => $dogtag_item);
		$dogtag_knot_data['action_view'] =
			$dogtag_knot_data['box_index']['action_view'] = "cms/dogtag/link_list_row_actions_view";
		$dogtag_knot_data['box_index']['foreign_keys'][$object_type] = $object_id;
		if ($object_type == "topic")
		    $dogtag_knot_data['box_index']['list_fields_2D'] = $dogtag_knot_data['list_fields_2D'] = $list_fields_2D = $dogtag_model->topic_link_list_fields_2D;
		else
		    $dogtag_knot_data['box_index']['list_fields_2D'] = $dogtag_knot_data['list_fields_2D'] = $list_fields_2D = $dogtag_model->link_list_fields_2D;
		$dogtag_knot_data['box_index']['list_fields'] = $dogtag_knot_data['list_fields'] = $list_fields = array_keys($list_fields_2D);
        # Bei Paketen und Hotels kein required/notwendig anzeigen, braucht man nur bei Themen-/Kategorie-Seiten
        if (in_array($object_type, ["package", "hotel"])){
            unset($data['box_index']['list_fields_2D']['required']);
            unset($data['box_index']['list_fields']['required']);
        }
		#
		return $this->get_view("cms/dogtag/list_row_view", $dogtag_knot_data);
	}

	public function todo_list($object_type, $object_id, $item = null){
		$output = array();
		$CI = get_instance();
		/** @var MY_Model $model */
		/** @var Dogtag_model $dogtag_model */
		$CI->load->model("cms/".$object_type."_model");
		$CI->load->model("cms/dogtag_model");
		$CI->load->model("cms/dogtag_knot_model");
        $dogtag_model = $CI->dogtag_model;
		$model = $CI->{$object_type."_model"};
		#
		$icon_success = '<i class="fa fa-check-circle-o text-success"></i>';
        $icon_fail = '<i class="fa fa-times-circle-o text-danger"></i>';
#
		if ($object_type == "producer"){
            $picture_choosen = $model->check_picture_choosen($object_id);
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/picture/index?hotel_id=$object_id&foreign_keys[hotel]=$object_id").'" onclick="return ajax_submit(this);">'.(($picture_choosen) ? '<i class="fa fa-check-circle-o text-success"></i>':$icon_fail).' Bild: Producer-Bild ausgewählt</a>';
        }
		if ($object_type == "hotel") {
		    $hotel_item = $model->get_item_by_id($object_id);
			$stars_choosen = $model->check_hotel_stars_choosen($object_id);
			$output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/hotel_attribute/index?class=stars&hotel_id=$object_id&foreign_keys[hotel]=$object_id").'" onclick="return ajax_submit(this);">'.(($stars_choosen) ? $icon_success:$icon_fail).' Sterne ausgewählt</a>';
			$producer_linked = $this->producer_model->get_items_by_knot($object_id, $model->table, "producer");
			$output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/producer/index?hotel_id=$object_id&foreign_keys[hotel]=$object_id").'" onclick="return ajax_submit(this);">'.((count($producer_linked)) ? $icon_success:$icon_fail).' Producer ausgewählt</a>';
//			$locations_linked = $this->location_model->get_items_by_knot($object_id, $model->table, "location");
//			$output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/location/index?hotel_id=$object_id&foreign_keys[hotel]=$object_id").'" onclick="return ajax_submit(this);">'.((count($locations_linked)) ? $icon_success:$icon_fail).' Lokation ausgewählt</a>';
            $locationtags_linked = $dogtag_model->get_items_by_knot($object_id, $model->table, "dogtag");
			$output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/location/index?hotel_id=$object_id&foreign_keys[hotel]=$object_id").'" onclick="return ajax_submit(this);">'.((count($locationtags_linked)) ? $icon_success:$icon_fail).' Lokation-Tag ausgewählt</a>';
			$packages_linked = $this->package_model->get_items_by_knot($object_id, $model->table, "package");
			$output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/package/index?hotel_id=$object_id&foreign_keys[hotel]=$object_id").'" onclick="return ajax_submit(this);">'.((count($packages_linked)) ? $icon_success:$icon_fail).' Paket angelegt/verlinkt</a>';
            $content_anreise_linked = $model->check_hotel_anreise_choosen($hotel_item, $model->table, "content");
//            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/content/index?hotel_id=$object_id&foreign_keys[hotel]=$object_id").'" onclick="return ajax_submit(this);">'.((count($packages_linked)) ? $icon_success:$icon_fail).' Anreise angelegt/verlinkt</a>';
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/content/create?hotel_id=$object_id&foreign_keys[hotel]=$object_id").'" onclick="return ajax_submit(this);">'.(($content_anreise_linked) ? $icon_success:$icon_fail).' Inhalt: Anreise angelegt</a>';
            $content_hotel_info_linked = $model->check_hotel_info_choosen($hotel_item, $model->table, "content");
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/content/create?hotel_id=$object_id&foreign_keys[hotel]=$object_id").'" onclick="return ajax_submit(this);">'.(($content_hotel_info_linked) ? $icon_success:$icon_fail).' Inhalt: Hotel Info kompakt angelegt</a>';
		}
		if ($object_type == "package"){
            $create_params = $object_type."_id=$object_id&foreign_keys[$object_type]=$object_id";
            #
            $producer_linked = $this->producer_model->get_items_by_knot($object_id, $model->table, "producer");
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/producer/index?$create_params").'" onclick="return ajax_submit(this);">'.((count($producer_linked)) ? $icon_success : $icon_fail).' Producer ausgewählt</a>';
            $gallery_choosen = $model->check_gallery_choosen($object_id);
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/gallery/index?$create_params").'" onclick="return ajax_submit(this);">'.(($gallery_choosen) ? $icon_success:$icon_fail).' Bilder-Galerie: Paket-Galerie ausgewählt</a>';
            $hotel_linked = $this->hotel_model->get_items_by_knot($object_id, $model->table);
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/hotel/index?$create_params").'" onclick="return ajax_submit(this);">'.((count($hotel_linked)) ? $icon_success:$icon_fail).' Hotel ausgewählt</a>';
            $content_story_linked = $model->check_content_type_choosen($object_id, "story");
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/content/create?$create_params&data[content][0][content_type]=story").'" onclick="return ajax_submit(this);">'.(($content_story_linked) ? $icon_success:$icon_fail).' Inhalt: Story angelegt</a>';
            $content_producer_tipp_linked = $model->check_content_type_choosen($object_id, "producer%tipp");
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/content/create?$create_params&data[content][0][content_type]=producertipp").'" onclick="return ajax_submit(this);">'.(($content_producer_tipp_linked) ? $icon_success:$icon_fail).' Inhalt: Tipp vom Producer agelegt</a>';
            $content_zlp_linked = $model->check_content_type_choosen($object_id, "Leistungen");
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/content/create?$create_params&data[content][0][content_type]=leistungen").'" onclick="return ajax_submit(this);">'.(($content_zlp_linked) ? $icon_success:$icon_fail).' Inhalt: Zeiten, Leistungen, Preise angelegt</a>';
        }
        if ($object_type == "topic"){
            $create_params = $object_type."_id=$object_id&foreign_keys[$object_type]=$object_id";
            # "Themen-Seite Kopf"
            $content_topichead_linked = $model->check_content_type_choosen($object_id, "topichead");
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/content/create?$create_params&data[content][0][content_type]=topichead").'" onclick="return ajax_submit(this);">'.(($content_topichead_linked) ? $icon_success:$icon_fail).' Inhalt: Seiten-Kopf angelegt</a>';
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/content/index?$create_params&data[content][0][content_type]=topichead").'" onclick="return ajax_submit(this);">'.(($content_topichead_linked) ? $icon_success:$icon_fail).' Inhalt: Seiten-Kopf verlinkt</a>';
            # Producer
            $producer_linked = $this->producer_model->get_items_by_knot($object_id, $model->table, "producer");
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/producer/create?$create_params").'" onclick="return ajax_submit(this);">'.((count($producer_linked)) ? $icon_success : $icon_fail).' Producer angelegt</a>';
            $output[] = '<a class="btn btn-sm btn-default" href="'.site_url("cms/producer/index?$create_params").'" onclick="return ajax_submit(this);">'.((count($producer_linked)) ? $icon_success : $icon_fail).' Producer ausgewählt</a>';
        }
        if ($object_type == "dogtag"){
            if (in_array(strtolower($item->class), array("location"))) {
                $filter = "&data[filter][location][name]=$item->name&data[filter][location][type]=$item->subclass";
                $create_params = $object_type."_id=$object_id&foreign_keys[$object_type]=$object_id".$filter;
                $location_object_linked = $this->location_model->get_items_by_knot($object_id, $model->table, "location");
                $output[] = '<a class="btn btn-sm btn-default" href="'
                    . site_url("cms/location/index?$create_params&data[dogtag][0][name]=" . @$item->name)
                    . '" onclick="return ajax_submit(this);">' . (($location_object_linked) ? $icon_success : $icon_fail) . ' Location Objekt verbinden</a>';
                $output[] = '<a class="btn btn-sm btn-default" href="'
                    . site_url("cms/location/create?$create_params&data[dogtag][0][name]=" . @$item->name)
                    . '" onclick="return ajax_submit(this);">' . (($location_object_linked) ? $icon_success : $icon_fail) . ' Location Objekt anlegen</a>';
            }
        }
        if ($object_type == "location"){
            $filter = "&data[filter][dogtag][name]=$item->name"
//                ."&data[filter][dogtag][title]=$item->title"
                ."&data[filter][dogtag][class]=location"
                ."&data[filter][dogtag][subclass]=$item->type";
            $create_params = $object_type."_id=$object_id&foreign_keys[$object_type]=$object_id".$filter;
            $location_dogtag_linked = $this->dogtag_model->get_items_by_knot($object_id, $object_type, "dogtag");
            $output[] = '<a class="btn btn-sm btn-default" href="'
                . site_url("cms/dogtag/index?$create_params&data[dogtag][0][name]=" . @$item->name)
                . '" onclick="return ajax_submit(this);">' . (($location_dogtag_linked) ? $icon_success : $icon_fail)
                . ' Location-Hundemarke verbinden</a>';
            $output[] = '<a class="btn btn-sm btn-default" href="'
                . site_url("cms/dogtag/create?$create_params&data[dogtag][0][name]=" . @$item->name)
                . '" onclick="return ajax_submit(this);">' . (($location_dogtag_linked) ? $icon_success : $icon_fail)
                . ' Location-Hundemarke anlegen</a>';
        }

        if (count($output) > 0)
		    return '<ul class="list-inline"><li>'.implode('</li><li>', $output).'</li></ul>';
        return "";
	}

    /**
     * @deprecated since 2016-12-02 use Model_helper_lib::get_flat_items() $model->mh()->get_flat_items()
     * @param $items
     * @param $key
     * @return array
     */
	public function get_flat_items($items, $key){
        foreach ($items as $k => $v){
            $arr[] = $items[$k]->{$key};
        }
        return $arr;
    }

    public function get_anchor_link($content_type_id, $content_id = null, $content_title = null) {
        if ($content_type_id == 14) {
            $anchor_link = "paket-leistungen";
        } else if ($content_type_id == 15) {
            $anchor_link = "preise";
        } else if ($content_type_id == 2) {
            $anchor_link = "hotel";
        } else if ($content_type_id == 114) {
            $anchor_link = "termine";
        } else if ($content_type_id == 103) {
            $anchor_link = "gaestebewertung";
        } else if ($content_type_id == 1) {
            $anchor_link = "story";
        } else if ($content_type_id == 4) {
            $anchor_link = "tippvomproducer";
        } else if ($content_type_id == 120) {
            $anchor_link = "pdfprospekt";
        } else if ($content_type_id == 5) {
            $anchor_link = "lage-anreise";
        } else if (strpos($content_title, "Flüge") !== FALSE) {
            $anchor_link = "fluege";
        } else {
	        $anchor_link = $content_id;
        }
        return $anchor_link;
    }

    function clear_string($str, $how = '-'){
        $search = array("ä", "ö", "ü", "ß", "Ä", "Ö",
            "Ü", "&", "é", "á", "ó",
            " :)", " :D", " :-)", " :P",
            " :O", " ;D", " ;)", " ^^",
            " :|", " :-/", ":)", ":D",
            ":-)", ":P", ":O", ";D", ";)",
            "^^", ":|", ":-/", "(", ")", "[", "]",
            "<", ">", "!", "\"", "§", "$", "%", "&",
            "/", "(", ")", "=", "?", "`", "´", "*", "'",
            "_", ":", ";", "²", "³", "{", "}",
            "\\", "~", "#", "+", ".", ",",
            "=", ":", "=)");
        $replace = array("ae", "oe", "ue", "ss", "Ae", "Oe",
            "Ue", "und", "e", "a", "o", "", "",
            "", "", "", "", "", "", "", "", "",
            "", "", "", "", "", "", "", "", "",
            "", "", "", "", "", "", "", "", "",
            "", "", "", "", "", "", "", "", "",
            "", "", "", "", "", "", "", "", "",
            "", "", "", "", "", "", "", "", "", "");
        $str = str_replace($search, $replace, $str);
        $str = strtolower(preg_replace("/[^a-zA-Z0-9]+/", trim($how), $str));
        return $str;
    }

}

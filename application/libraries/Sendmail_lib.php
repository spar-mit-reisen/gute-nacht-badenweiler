<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Sendmail_lib
{
    /**
     * Sendmail_lib constructor.
     */
    public function __construct()
    {
        ci()->load->library(array('email'));
    }

    /**
     * Sendet E-Mail vom angegebenen Typ
     * @param null $formtype
     * @param null $formdata
     */
    public function sendMail($formtype = null, $formdata = null) {

        $mail_config['mailtype'] = 'html';
        $mail_config['protocol'] = 'smtp';
        $mail_config['smtp_host'] = 'mail.spar-mit.com';
        $mail_config['smtp_user'] = 'terrine@spar-mit.com';
        $mail_config['smtp_pass'] = 'ImAp,,1234%';
        $mailData = array();

        foreach ($formdata as $key => $value) {
            $formdata[$key] = htmlspecialchars($value);
        }

        if (@$formdata['email'] != "sample@email.tst") {

            if ($formtype == 'test') {
                $mail_subject = 'Testmail von Spar mit! Reisen';
                $mailData['header'] = 'Testmail';
                $mailData['content'] = 'Dies ist eine Testmail';
                $mail_message = ci()->get_view('mail/mail_view', array('mailData' => $mailData));
                ci()->email->initialize($mail_config);
                ci()->email->from('kontakt@spar-mit.com', 'Spar mit! Reisen');
                #ci()->email->to('h.prinke@spar-mit.com');
                ci()->email->to('marcoheine@me.com');
                ci()->email->subject($mail_subject);
                ci()->email->message($mail_message);
                ci()->email->send();
            } elseif ($formtype == 'kontakt_intern') {
                $mail_config['mailtype'] = 'text';
                $mail_subject = 'Anfrage über Kontaktformular';
                $mail_message = ci()->get_view('mail/messages/mail_kontakt_intern', array('formdata' => $formdata));
                ci()->email->initialize($mail_config);
                ci()->email->from('kontakt@spar-mit.com', 'Gute Nacht Badenweiler');
                ci()->email->to('h.prinke@spar-mit.com', 'Gute Nacht Badenweiler');
                ci()->email->to('info@gute-nacht-badenweiler.de', 'Gute Nacht Badenweiler');
                ci()->email->subject($mail_subject);
                ci()->email->message($mail_message);
                ci()->email->send();
            } elseif ($formtype == 'kontakt_extern') {
                $mail_subject = 'Ihre Anfrage über das Kontaktformular bei Spar mit! Reisen';
                $mailData['header'] = 'Anfrage über Kontaktformular';
                $mailData['content'] = ci()->get_view('mail/messages/mail_kontakt_extern', array('formdata' => $formdata));
                $mail_message = ci()->get_view('mail/mail_view', array('mailData' => $mailData));
                ci()->email->initialize($mail_config);
                ci()->email->from('kontakt@spar-mit.com', 'Spar mit! Reisen');
                ci()->email->to(@$formdata['email']);
                ci()->email->subject($mail_subject);
                ci()->email->message($mail_message);
                if (filter_var(@$formdata['email'], FILTER_VALIDATE_EMAIL)) {
                    ci()->email->send();
                }
            }
        }
    }
}

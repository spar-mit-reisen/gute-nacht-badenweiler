<?php

/**
 * Mailingwork
 * @see https://mailingwork.de
 */
class Mailingwork
{
    private $api_url;
    private $listId;
    private $optinSetupId;
    private $password;
    private $username;

    const EMAIL = 'email';
    const FIELDS = 'fields';
    const FIRSTNAME = 'firstname';
    const LASTNAME = 'lastname';
    const LIST_ID = 'listId';
    const OPTIN_SETUP_ID = 'optinSetupId';
    const PASSWORD = 'password';
    const TITLE = 'title';
    const USERNAME = 'username';

    /**
     * Mailingwork constructor.
     */
    public function __construct()
    {
        $this->api_url = 'https://login.mailingwork.de/webservice/webservice/json/optinrecipient';
        $this->listId = 149;
        $this->optinSetupId = 1;
        $this->password = 'spar-mit123!';
        $this->username = 'spar-mit';
    }

    /**
     * @param $form_data
     * @return array
     */
    public function prepare_data_to_send($form_data)
    {
        $arr = array(
            1 => $form_data[self::EMAIL],
            3 => $form_data[self::FIRSTNAME],
            4 => $form_data[self::LASTNAME]
        );
        if (isset($form_data[self::TITLE])) {
            $arr[2] = $form_data[self::TITLE];
        }
        return $arr;
    }

    /**
     * @param $form_data
     */
    public function send($form_data)
    {
        $request_params = array(
            self::USERNAME => $this->username,
            self::PASSWORD => $this->password,
            self::OPTIN_SETUP_ID => $this->optinSetupId,
            self::LIST_ID => $this->listId,
            self::FIELDS => $this->prepare_data_to_send($form_data)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request_params));
        $response = curl_exec($ch);
        curl_close($ch);
//        return $response;
    }
}
<?php

/**
 * User: sbraun
 * Date: 27.01.17
 * Time: 12:21
 */
class Mobile_detect_lib
{

    /** @var Mobile_Detect $mobile_detect */
    private $mobile_detect;

    function __construct() {
        ci()->load->library('session');
    }

    /**
     * @return Mobile_Detect
     */
    private function get_lib(): Mobile_Detect {
        if (is_null($this->mobile_detect)) {
//            require_once FCPATH."/Vendor/".'Mobile-Detect/Mobile_Detect.php';
            require_once APPPATH . "/third_party/" . 'Mobile-Detect/Mobile-Detect/Mobile_Detect.php';
            $this->mobile_detect = new Mobile_Detect();
        }
        return $this->mobile_detect;
    }

    /**
     * @param string $return_type js|bool
     * @return bool|string
     */
    public function is_mobile(string $return_type = "bool") {
        if (isset($_SESSION['scope']['is_mobile']))
            $is_mobile = $_SESSION['scope']['is_mobile'];
        else {
            $is_mobile = ($this->get_lib()->isMobile() && !$this->get_lib()->isTablet());
            $_SESSION['scope']['is_mobile'] = $is_mobile;
        }
        if ($is_mobile)
            return ($return_type == "js") ? "true" : true;
        else
            return ($return_type == "js") ? "false" : false;
    }


    /**
     * @param string $return_type js|bool
     * @return bool|string
     */
    public function is_tablet(string $return_type = "bool") {
        if (isset($_SESSION['scope']['is_tablet']))
            $is_tablet =  $_SESSION['scope']['is_tablet'];
        else {
            $is_tablet = $this->get_lib()->isTablet();
            $_SESSION['scope']['is_tablet'] = $is_tablet;
        }
        if ($is_tablet)
            return ($return_type == "js") ? "true" : true;
        else
            return ($return_type == "js") ? "false" : false;
    }

    /**
     * @param string $return_type js|bool
     * @return bool|string
     */
    public function is_desktop(string $return_type = "bool") {
        if ($this->is_mobile() || $this->is_tablet())
            return ($return_type == "js") ? "false" : false;
        return ($return_type == "js") ? "true" : true;
    }

}
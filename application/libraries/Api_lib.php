<?php

/**
 * User: sbraun
 * Date: 23.03.17
 * Time: 14:23
 */
class Api_lib
{
    /**
     * @param array $url_segments a/b/c/d
     * @param array $get_params key=>value pairs
     * @param array $post_params
     * @return Curl_response api-return-data
     */
    public function api_return($url_segments = [], array $get_params = [], array $post_params = []){
//        $limit_from = (get_instance()->input->post_get("limit_from")) ? (int) get_instance()->input->post_get("limit_from") : 0;
//        if ($limit_from) {
//            $params['limit_from'] = $limit_from;
//        }
//        if (class_exists('Memcached'))
//            ci()->load->driver('cache', ['adapter' => 'memcached']);
//        else
//            ci()->load->driver('cache', ['adapter' => 'file']);

//        if (isset($this->cache_manager_lib)) {
//            ci()->cache = $this->cache_manager_lib;
//        }
//        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

//        if (ci()->cache && !ci()->curl_lib()->cache_handler)
//            ci()->curl_lib()->cache_handler = ci()->cache;
        #ci()->curl_lib()->cache_handler = ci()->cached_json_model();
        return ci()->curl_lib()->get_api_data($url_segments, [], $get_params, $post_params);
    }
}
<?php
/**
 * User: pdahlenburg
 * Date: 10.04.17
 * Time: 11:10
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Search_lib
{
    public $libraries = array('curl_lib');

    public function __construct() {
//        $this->CI = &get_instance();
    }

    public function setOptions()
    {
        unset($_SESSION['detailsearch']);
        $detailsearch_options = [];
        $view_data = $this->getOptions();
        if (isset($view_data)) {
          // $this->ausgeben($view_data);
         // die;
            $counter=0;
            foreach ($view_data as $options) {
                if(is_array($options)) {
                    foreach ($options as $option){
                        if (isset($option['name']) && isset($option['values'])) {
                            $_SESSION['detailsearch']['extras'][$counter]["name"]=$option['name'];
                            $_SESSION['detailsearch']['extras'][$counter]["values"]=$option['values'];
                            $counter+=1;
                        }
                    }

                }

            }
            $counter=0;
             foreach($view_data["categories"] as $options) {
                 if (isset($options["id"]) && isset($options["name"])) {
                     $_SESSION['detailsearch']['categories'][$counter]["id"] = $options["id"];
                     $_SESSION['detailsearch']['categories'][$counter]["count"] = $options["count"];
                     $_SESSION['detailsearch']['categories'][$counter]["name"] = $options["name"];
                     $_SESSION['detailsearch']['categories'][$counter]["values"] = "1";
                     $counter += 1;
                 }
             }
            $counter=0;
            foreach($view_data["countrys"] as $options) {


               if(isset($options["properties"]["id"])) {

                   $_SESSION['detailsearch']['countrys'][$counter]["properties"]["id"] = $options["properties"]["id"];
                   $_SESSION['detailsearch']['countrys'][$counter]["properties"]["count"] = $options["properties"]["count"];
                   $_SESSION['detailsearch']['countrys'][$counter]["properties"]["title"] = $options["properties"]["title"];
                   $_SESSION['detailsearch']['countrys'][$counter]["properties"]["values"] = "1";
                   $counterlog=0;
                   foreach ($options["locations"] as $location) {


                       if (isset($location)&&isset($location["location_id"])) {
                           $_SESSION['detailsearch']['countrys'][$counter]["locations"][$counterlog]["location_id"] = $location["location_id"];
                           $_SESSION['detailsearch']['countrys'][$counter]["locations"][$counterlog]["count"] = $location["count"];
                           $_SESSION['detailsearch']['countrys'][$counter]["locations"][$counterlog]["title"] = $location["title"];
                           // $_SESSION['detailsearch']['countrys'][properties]["values"] = "1";
                       }
                       $counterlog+=1;
                   }
                   $counter += 1;
               }
            }


        }
    }
    public function getOptions() {
       // $limit_from = ($this->input->post_get('limit_from')) ? (int)$this->input->post_get('limit_from') : 0;
        $limit_from = 10000;
        $searchoptions = array(
            'SEARCH_FULLTEXT' => '',
            'SEARCH_ZIP' => '',
            'SEARCH_RANGE' => '',
            'MIN_DATE' => '',
            'MAX_DATE' => '',
            'LIMIT_FROM' => $limit_from,
            'PACKAGES' => 'off'
        );
        $api_data_string = ci()->curl_lib()->get_search_data([], [], $searchoptions);
        $api_data = json_decode($api_data_string, true);
        return $api_data;
    }




       /* if (isset($request_data)) {
            foreach ($request_data as $options) {
                $options = json_decode($options, true);
                foreach ($options as $option) {


                    if($option['foldname'] == 'plzRange'){
                        $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['foldname'] = "entfernung";
                        $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['value'] ="entfernung";
                        $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['inputname'] = "entfernung";
                    }

                    elseif (($option['foldname'] != 'reisetermin' && $option['value'] == 1) || ($option['foldname'] == 'reisetermin' && !empty($option['value']))) {
                        $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['foldname'] = $option['foldname'];
                        $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['inputname'] = $option['inputname'];
                        $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['value'] = $option['value'];
                        $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['id'] = $option['id'];
                        if (isset($option['country_id'])) {
                            $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['country_id'] = $option['country_id'];
                        }
                    }
                    elseif($option['foldname'] == 'inputCity'){

                        $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['foldname'] = $option['foldname'];
                        $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['inputname'] = $option['value'];
                        $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['value'] =$option['value'];
                        $_SESSION['detailsearch'][$option['foldname']][$option['inputname']]['id'] = $option['id'];
                    }

                    else {
                        unset($_SESSION['detailsearch'][$option['foldname']][$option['inputname']]);
                        if (empty($_SESSION['detailsearch'][$option['foldname']])) {
                            unset($_SESSION['detailsearch'][$option['foldname']]);
                        }
                        if (empty($_SESSION['detailsearch'])) {
                            unset($_SESSION['detailsearch']);
                        }

                        ### todo: Region löschen, wenn Parent-Country gelöscht wurde
                        unset($_SESSION['detailsearch'][$option['foldname']][$option['inputname']]);
                    }
                }



            }

        }
        $this->data = array();


        if (!empty($_SESSION['detailsearch'])) {
            foreach ($_SESSION['detailsearch'] as $selections) {
                $append = array();

                foreach ($selections as $selection) {
                    if (is_array($selection)) {
                        if ($selection['inputname'] == 'plzRange') {
                            $append[] = '<span>' . $selection['value'] . 'km</span>';
                        }
                        elseif ($selection['inputname'] == 'plz') {
                            $append[] = '<span>' . $selection['value'] . '</span>';
                        }

                        else if ($selection['value'] == 1) {
                            $inputname = ci()->search_lib->translateOptions($selection['inputname']);
                            $append[] = '<span>' . $inputname . '</span>';
                        } else if ($selection['inputname'] == 'min_date' && !empty($selection['value'])) {
                            $append[] = '<span>von ' . $selection['value'] . '</span>';
                        } else if ($selection['inputname'] == 'max_date' && !empty($selection['value'])) {
                            $append[] = '<span>bis ' . $selection['value'] . '</span>';
                        }
                        else if ($selection['foldname'] == 'inputCity') {
                            $append[] = '<span>bis ' . $selection['value'] . '</span>';
                        }

                        $this->data['html']['html']['.detailsearch .selection .' . $selection['foldname']] = $append;

                        $limit_from = ($this->input->post_get('limit_from')) ? (int)$this->input->post_get('limit_from') : 0;
                        $searchOptions = array(
                            'SEARCH_FULLTEXT' => ($selection['inputname']),
                            'SEARCH_ZIP' => '',
                            'SEARCH_RANGE' => '',
                            'MIN_DATE' => '',
                            'MAX_DATE' => '',
                            'LIMIT_NUM' => $this->package_api_model()->limit_num,
                            'LIMIT_FROM' => $limit_from);
                        $api_data_string = $this->curl_lib()->get_search_data([], [], $searchOptions);
                        $api_data = json_decode($api_data_string, true);

                        for($i=0;$i<10;$i++){
                            $data = array(
                                "packagenumber" => $api_data['packages'][0]['package_number']
                            );
                            echo $this->load->view("suche/lazy-package_view", $data, true);
                        }

                    }

                }

            }

            if (empty($append)) {
                $append[] = null;
                $this->data['html']['fadeIn']['.detailsearch .selection'] = '';

            } else {
                $this->data['html']['fadeIn']['.detailsearch .selection'] = '';
            }

        }



    }*/
    /**
    * Übersetze Optionen
    **/
    public function translateOptions($option = null) {
        $optionWording['BREWERY_HOTEL'] = 'Brauerei-Hotels'; //Brauerei-Hotels
        $optionWording['HOLIDAY_FLAT'] = 'Ferienwohnungen'; //Ferienwohnungen
        $optionWording['MEDIEVAL_FLAVORED_HOTEL'] = 'Schlosshotels/Burghotels'; //Schlosshotels/Burghotels
        $optionWording['UNKNOWN_HOTEL_TYPE'] = 'Keine Klassifizierung'; //Keine Klassifizierung
        $optionWording['SPORT_HOTEL'] = 'Sporthotel'; //Sporthotels
        $optionWording['BEAUTY'] = 'Beauty'; //Beauty
        $optionWording['DISABLED_READY'] = 'Behindertengerecht'; //Behindertengerecht
        $optionWording['TURC'] = 'Dampfbad'; //Dampfbad
        $optionWording['BICYCLE_RENTAL'] = 'Fahrradverleih'; //Fahrradverleih
        $optionWording['GYM'] = 'Fitnessraum'; //Fitnessraum
        $optionWording['COMMON_PETS_ALLOWED'] = 'Hunde erlaubt';
        $optionWording['COMMON_PETS_DISALLOWED'] = 'Hunde nicht erlaubt';
        $optionWording['OWNER_DIRECTED'] = 'Inhabergeführtes Privathotel'; //Inhabergeführtes Privathotel
        $optionWording['LIFT'] = 'Lift'; //Lift
        $optionWording['MASSAGE'] = 'Massage'; //Massage
        $optionWording['PARKINGLOT'] = 'Parkplatz'; //Parkplatz
        $optionWording['GARAGE'] = 'Parkplatz in Garage'; //Parkplatz in Garage
        $optionWording['POOL_OUTSIDE'] = 'Pool außen'; //Pool außen
        $optionWording['POOL_INSIDE'] = 'Pool innen'; //Pool innen
        $optionWording['SAUNA'] = 'Sauna'; //Sauna
        $optionWording['SOLARIUM'] = 'Solarium'; //Solarium
        $optionWording['WHIRLPOOL'] = 'Whirlpool'; //Whirlpool
        $optionWording['MORE_CHILDREN'] = '2 Kinder oder mehr im Zustellbett möglich'; //2 Kinder oder mehr im Zustellbett möglich
        $optionWording['THIRD_PERSON'] = '3. Person im Zimmer'; //3. Person im Zimmer
        $optionWording['BALKON'] = 'Balkon/Terrasse'; //Balkon/Terrasse
        $optionWording['FREE_SINGLE_ROOM'] = 'Einzelzimmer ohne Zuschlag'; //Einzelzimmer ohne Zuschlag
        $optionWording['DOUBLE_ROOM'] = 'Familienzimmer/DZ mit zweitem Raum'; //Familienzimmer/DZ mit zweitem Raum
        $optionWording['AC'] = 'Klimaanlage'; //Klimaanlage
        $optionWording['SAFE'] = 'Safe'; //Safe
        $optionWording['SINGLE_PARENT_OFFER'] = 'Single mit Kind-Tarif'; //Single mit Kind-Tarif
        $optionWording['WIFI'] = 'WLAN'; //WLAN
        $optionWording['CITY_CENTRE_NEARBY'] = 'Altstadt/Zentrum in der Nähe (bis 2 km)'; //Altstadt/Zentrum in der Nähe (bis 2 km)
        $optionWording['BEACH_NEARBY'] = 'Badestrand in der Nähe (bis 500 m)'; //Badestrand in der Nähe (bis 500 m)
        $optionWording['TRAIN_STATION_NEARBY'] = 'Bahnhof in der Nähe (bis 2 km) o. Abholservice'; //Bahnhof in der Nähe (bis 2 km) oder Gratis-Abholservice für Bahnreisende
        $optionWording['AMUSEMENT_PARK_NEARBY'] = 'Freizeitpark in der Nähe (bis 20 km)'; //Freizeitpark in der Nähe (bis 20 km)
        $optionWording['SKILIFT_NEARBY'] = 'Skilift in der Nähe (bis 500 m)'; //Skilift in der Nähe (bis 500 m)
        $optionWording['THERMAL_BATH_NEARBY'] = 'Thermalbad in der Nähe (bis 20 km)'; //Thermalbad in der Nähe (bis 20 km)
        $optionWording['ONE_STAR'] = '1 - 1,5 Sterne'; // Anzahl Sterne - 1 Stern
        $optionWording['TWO_STARS'] = '2 - 2,5 Sterne'; // Anzahl Sterne - 2 Sterne
        $optionWording['THREE_STARS'] = '3 - 3,5 Sterne'; // Anzahl Sterne - 3 Sterne
        $optionWording['FOUR_STARS'] = '4 - 4,5 Sterne'; // Anzahl Sterne - 4 Sterne
        $optionWording['FIVE_STARS'] = '5 Sterne'; // Anzahl Sterne - 5 Sterne

        if (isset($optionWording[strtoupper($option)])) {
            return $optionWording[strtoupper($option)];
        } else {
            return $option;
        }
    }

}
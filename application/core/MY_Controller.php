<?php
require_once APPPATH . "/core/traits/MY_Controller_Links.php";
require_once APPPATH . "/core/traits/MY_Controller_Output.php";
/**
 * Description of MY_Controller
 *
 * @author sebra
 */
class MY_Controller extends CI_Controller
{
    use MY_Controller_Links;
    use MY_Controller_Output;

    public $default_header = 'common/header';
    public $default_footer = 'common/footer';
    public $templateDir = null;
    public $default_helpers = array();
    public $default_libraries = array('session','mobile_detect_lib');
    public $libraries = array();
    public $helpers = array();
    public $default_models = array();
    public $models = array();

    /**
     * @var bool
     * true is active
     */
    public $auth_ldap_status = false;

    /** @var CI_Loader */
    public $load;
    /** @var string */
    public $request_type;
    /** @var Mobile_detect_lib */
    public $mobile_detect_lib;
    /** @var CI_Config */
    public $config;
    /** @var MY_Output */
    public $output;

    public $no_overhead = true;
    public $cms_session_id = null;

    public function __construct() {
        parent::__construct();
        $this->config->set_item('language', 'english');
        $this->load->helper($this->default_helpers);
        $this->load->helper($this->helpers);
        $this->load->library($this->default_libraries);
        $this->load->library($this->libraries);
        if ($this->input->get_post("profiler")) {
            $this->output->enable_profiler(TRUE);
            $this->save_queries = TRUE;
        }
        $this->load->model($this->default_models);
        $this->load->model($this->models);

        if ( $this->auth_ldap_status ) {
            $this->load->library("auth_ldap");
            $this->auth_ldap->is_authenticated();
        }
        # reset mobile-detect session switch
        if ($this->input->post_get("reset_mobile_detect")) {
            unset($_SESSION['scope']['is_mobile']);
            unset($_SESSION['scope']['is_tablet']);
            unset($_SESSION['cms_session_id']);
        }
        $this->request_type = $this->input->post_get("request_type");
        $this->cms_session_id = $this->input->post_get("cms_session_id");
        # @todo check cms_session_id if valid! if not, delete property!
        if ($this->cms_session_id)
            $_SESSION['cms_session_id'] = $this->cms_session_id;
        if (@$_SESSION['cms_session_id'] && is_null($this->cms_session_id))
            $this->cms_session_id = $_SESSION['cms_session_id'];

        # Client-depending Browser Switch Mobile/Desktop
        if (!in_array(get_class($this), ["Media",'Main']) &&
            !$this->config->item('allow_desktop_viewport')
            && !$this->mobile_detect_lib->is_mobile()
            && !$this->cms_session_id
            && !$this->is_ajax_request()
        ) {
            if (in_array(@$this->where_am_i()['class'], ['topic','package','paket']))
                $this->redirect_to_desktop = true;
            else {
//              echo "goto: ". "https://www.spar-mit.com/";
//              die;
                redirect("https://www.spar-mit.com");
            }
        } else {
            $this->redirect_to_desktop = false;
            $this->desktop_url = "";# @todo desktop url aus datenbank
        }
//        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
    }

    private function get_request_type() {
        # set request_type
        $this->request_type = ($this->input->post_get("request_type")) ? $this->input->post_get("request_type") : $this->request_type;
        if (is_null($this->request_type) || $this->request_type == "html")
            if (is_cli())
                $this->request_type = "console";
            elseif (stristr(@$_SERVER['HTTP_REFERER'], "request_type=iframe"))
                $this->request_type = "iframe";
            else
                $this->request_type = "html";

        if (($this->server_name == "localhost") || $this->request_type == 'console') {
            $this->auth_ldap_status = false;
        }
        if ($this->request_type == 'console' && in_array('session', $this->default_libraries))
            $this->default_libraries = array_diff($this->default_libraries, ['session']);
    }

    /**
     * outputs the page (header-output-footer),
     * typically the last step in a Controller->action()-method
     * you can use this in combination with one or multiple ..
     * $output[] = get_view('template');
     * .. calls.
     * @param string|array $output (content of the page)
     */
    public function show_page($output = "") {
        if ( is_array($output) ) {
            $data['output'] = implode("\n", $output);
        } elseif ( is_string($output) ) {
            $data['output'] = $output;
        }
        if (!isset($this->data))#avoid error
            $this->data = array();

        $this->load->view($this->default_header, $this->data);
        $this->load->view("output", $data);
        $this->load->view($this->default_footer, $this->data);
    }


    /**
     * TODO einpacken
     * @param array $path_to_view
     */
    public function load_templates($path_to_view)
    {
        foreach ($path_to_view as $key => $value) {
            (!empty($value))? $this->load->view($key, $value) : $this->load->view($key);
        }
    }

    /**
     * shorthand / alias for show_ajax() with inlcluded show_messages() after $this->message,
     * if the message sould send by ajax to the client.
     *
     * @param array $data expects array which is diven to the output as json
     *
     * @return object|string
     */
    public function show_ajax_message($data = null){
        if ( is_null($data) )
            $data &= $this->data;
        $data['html']['prepend']['.message_area'] = $this->bootstrap_lib->show_messages(null, null);
        return $this->show_ajax($data);
    }

    public function show_modal($data = null){
        if ( is_null($data) ) {
            $data = $this->data;
        }
        $data2['data'] = $data;
        $output = $this->get_view("boxes/modal/modal", $data);
        $data_json['html']['prepend']['body'] = $output;
        $this->show_ajax($data_json);
    }

    public function getTemplateDir() {
        if ( !is_null($this->templateDir) ) {
            return $this->templateDir;
        }
        $class = get_class($this);
        return strtolower($class);
    }

    /**
     * @return bool
     */
    public function is_ajax_request():bool {
        if (is_null($this->request_type))
            $this->get_request_type();
        return ($this->request_type == "ajax")? true : false;
    }

    public function get_calling_method_name($step = 2) {
        #way A
//		debug_backtrace()[1]['function'];
        #way B
        $e = new Exception();
        $trace = $e->getTrace();
        //position 0 would be the line that called this function so we ignore it
        $last_call = $trace[$step]['function'];
//		var_dump($last_call);
        return ($last_call);
    }

    /**
     * @return string e.g. "content"
     */
    public function get_controller_name() {
        return strtolower(get_class($this));
    }

    /**
     * @return string e.g. cms
     */
    public function get_controller_path() {
        return (isset($this->controller_path)) ? $this->controller_path : "";
    }

    /**
     * @return string e.g. cms/content
     */
    public function get_full_controller_name() {
        return $this->get_controller_path() . $this->get_controller_name();
    }

    /**
     *
     * @param string $text Message-Text
     * @param string $type info|warning|danger|success
     * @param string $context default
     * @param int $time_out_ms time in ms, after which the message should disappear automatically
     * @link https://getbootstrap.com/components/#alerts Bootstrap
     */
    public function message($text, $type = "info", $context = "default", $time_out_ms = 0) {
        if ( is_null($context) )
            $context = "default";
        $this->data['messages'][$context][] = (object) array("text" => $text, 'type' => $type);
//		$GLOBALS['messages'][$context][] = (object) array("text" => $text, 'type' => $type);
        $_SESSION['messages'][$context][] = (object) array("text" => $text, 'type' => $type, "time_out_ms" => $time_out_ms);
    }

    /**
     * returns the current logged in username
     * @return string username
     */
    public function get_username() {
        return @$_SESSION['logged_in']['cn'];
    }

    /***
     * @return array of path components
     */
    public function where_am_i() {
        $arr[0] = $this->uri->segment(0); #dir??
        $arr[1] = $this->uri->segment(1); #dir?
        $arr[2] = $this->uri->segment(2); #dir
        $arr[3] = $this->uri->segment(3); #controller
        $arr[4] = $this->uri->segment(4); #action

        $arr['directory'] = $this->router->directory;
        $arr['class'] = $this->router->class;
        $arr['method'] = $this->router->method;
        $arr['uri_string'] = $this->router->uri->uri_string;
//        var_dump($this->router);
//		$arr[$i++] = @$this->router->fetch_module(); //Module Name if you are using HMVC Component;
//		$this->router->fetch_class(); // class = controller
//		$this->router->fetch_method();

        $arr['path_to_controller'] = $this->router->directory . $this->router->class;
        $arr['path_to_action'] = $this->router->directory . $this->router->class . "/" . $this->router->method;

        return $arr;
    }

    /**
     * In list views ( index() ) you need to load the items linked to it.
     * @param array $items array of items/beans/row-objects which will be filled with their linked objects triggered by input-variables
     * @param object|null $model [optional] if null: $this->model is used
     */
    public function load_linked_data_for_foreign_keys(&$items, $model = null){
        if (is_null($model))
            $model = $this->model;
        foreach ($model->linked_by_knot as $object_type ){
            if ( is_numeric($this->input->get_post($object_type)) ) {
                $this->data['box_index']['foreign_keys'][$object_type] = (int) $this->input->get_post($object_type);
                $model->load($object_type, $items);
            }
            if ( is_numeric($this->input->get_post($object_type.'_id')) ) {
                $this->data['box_index']['foreign_keys'][$object_type] = (int) $this->input->get_post($object_type.'_id');
                $model->load($object_type, $items);
            }
        }
    }

    /**
     * prepares index-list
     */
    public function box_index()
    {
        $this->data = array();

        $items = $this->model->get_items();
        $this->model->get_data_for_list($items, 0, $this->data);
        $this->data['box_index']['title'] = "Liste " . $this->model->items_label;

        $this->load_linked_data_for_foreign_keys($items);
    }

    /**
     * default index view for modal
     */
    public function index_modal() {
        $this->data = array();
        $output = array();
//		$this->data['box_index']['hide_actions'] = true;
//		$this->data['box_index']['hide_header'] = true;
        $this->box_index();
        $this->data['box_index']['title'] = false;
        $this->data['box_index']['hide_actions'] = true;
        $output[] = $this->get_view("cms/list_view", $this->data);#custom view

//		$output[] = $this->bootstrap_lib->edit_box($item, $this->data, $this->location_model, $params);
//		$this->data['box_modal']['modal']['footer']['view'] = "empty_view";
        $this->data['box_modal']['modal_title'] = $this->model->item_label." auswählen";
        $this->data['box_modal']['modal_content'] = implode("", $output);
        $this->data['box_modal']['modal']['size'] = "modal-lg";
        $this->data['box_modal']['modal']['footer']['view'] = "empty_view";

        $this->show_modal();
    }

    /**
     * default create view for modal
     */
    public function create_modal($item = null){
        $model = $this->model;
        if (empty($this->data))
            $this->data = array();

        if ($item) {
            $id = 0; #new item
            if (!$id || $id == 0) {
                $item = $model->get_empty_item();
                $item->id = 0;
            } elseif (is_numeric($id)) {
                $item = $model->get_item_by_id($id);
            } else {
                $this->message("$model->item_label mit ID: $id ist ungültig.");
            }

            $offerer_id = $this->input->post_get("offerer_id");
            if (is_numeric($offerer_id)) {
                $item->offerer_id = $offerer_id;
                $this->model->backend_fields["offerer_id"] = array("type" => "hidden");
            }
            $package_id = $this->input->post_get("package_id");
            if (is_numeric($package_id)) {
                $this->data['package_id'] = $package_id;
                $item->package_id = $package_id;
                $this->content_model->backend_fields["package_id"] = array("type" => "hidden");
            }

            $foreign_keys = $this->input->post_get("foreign_keys");
            if (is_array($foreign_keys)) {
                if (!is_array($item->foreign_keys))
                    $item->foreign_keys = array();
                foreach ($foreign_keys as $k => $v) {
                    $item->foreign_keys[$k] = $v;
                }
            }
        }

        $params = array();
        $this->data['box_edit']['hide_actions'] = false;
        $this->data['box_edit']['hide_header'] = true;

        $output[] = $this->bootstrap_lib->edit_box($item, $this->data, $this->model, $params);
//		$this->smr_breadcrumbs->push('Location anlegen', '#');
//		$this->data['breadcrumbs'] = $this->smr_breadcrumbs->show();
        $this->data['box_modal']['modal']['footer']['view'] = "empty_view";
        $this->data['box_modal']['modal_title'] = $this->model->item_label." anlegen";
        $this->data['box_modal']['modal_content'] = implode("", $output);
        $this->show_modal();
    }

}

//function &ci(): MY_Controller {
//    return MY_Controller::get_instance();
//}
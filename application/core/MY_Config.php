<?php

/**
 * User: sbraun
 * Date: 27.01.17
 * Time: 16:55
 */
class MY_Config extends CI_Config
{
    public function site_url($uri = '', $protocol = NULL) {
        if (is_null($protocol))
            $protocol = $this->item('protocol');
        return parent::site_url($uri, $protocol);
    }
}
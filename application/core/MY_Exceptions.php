<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions {

    public function show_404($page = '', $log_error = TRUE) {
        ci()->output->set_status_header('404');
        ci()->smr_breadcrumbs->push('Fehler 404 - Seite nicht gefunden', ' ');
        $data['breadcrumbs'] = ci()->smr_breadcrumbs->show();

        $data['site_title'] = 'Seite nicht gefunden - Fehler 404 - Spar mit! Reisen';
        $data['site_description'] = 'Fehler 404 - Seite nicht gefunden';

        ci()->load_templates(array(
            'common/header' => $data,
            'common/error404_view' => $data,
            'common/footer' => []
        ));

        echo ci()->output->get_output();
        exit(4);
    }

}
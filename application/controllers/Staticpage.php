<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Staticpage extends MY_Controller
{

    public $helpers = array('url');
    public $libraries = array('curl_lib', 'smr_breadcrumbs', 'packagestripe_lib', 'sendmail_lib', 'media_lib');

    public function index() {

        $staticpage_subtype = $this->uri->segment(3);
        $staticpage = $this->uri->segment(2);

        $data['staticpage'] = $staticpage;
        $data['staticpage_subtype'] = $staticpage_subtype;
        $api_data_string = $this->fake_api_lib()->api_return(['getStaticpagesData', $staticpage], [], []);
        $api_data = json_decode($api_data_string, true);

        if (is_null($api_data) && !empty($api_response)) {
            echo "<pre>";
            var_dump($api_data);
//            var_dump($api_response);
            var_dump($this->curl_lib->error);
            echo "</pre>";
            die("Fehler in JSON! (Backend)");
        }

        if (isset($api_data) && is_array($api_data)) {

            # meta-tags
            $data['site_title'] = ucfirst($staticpage) .' - Spar mit! Reisen';
            $data['site_description'] = ucfirst($staticpage) . ' Spar mit! Reisen ✔ 365 Tage im Jahr erreichbar ✔ Deutschlands Reiseveranstalter Nr. 1';

            $data['staticpageData'] = $api_data;

            if ($staticpage == "jobs") {
                if (!$staticpage_subtype) {
                    $view = "jobs/jobs_view";
                } else if ($staticpage_subtype) {
                    $view = "jobs/job-single_view";
                }
            } else if ($staticpage == "team") {
                if (!$staticpage_subtype) {
                $view = "ueber-uns/team_view";
                } else if ($staticpage_subtype) {
                    $view = "ueber-uns/team/team-department-wrapper_view";
                }
            } else if ($staticpage == "reisegutschein") {
                if (empty(array_filter($_POST))) {
                    $view = "reisegutschein/reisegutschein_view";
                } else {
                    $view = "reisegutschein/reisegutschein-sent_view";
                    $this->reisegutschein();
                }
            } else {
                $view = "staticpage/staticpage_view";
            }

            $this->smr_breadcrumbs->push(ucfirst($staticpage), ' ');
            $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                $view => $data,
                'common/footer' => []
            ));
        } else {
            $no_redirect_found = true;
            # check for redirect in db
            if (1) {
                $uri_string = $this->router->uri->uri_string;
                $url_segments = $this->uri->segment_array();
                $redirect = $this->redirect_api_model()->check_redirect_url($uri_string);
                if (is_object($redirect))
                    $redirect_target = $redirect->new_controller . "/" . $redirect->new_slug;
                elseif (is_array($redirect) && !empty($redirect))#multiple matches - should not occure!
                    $redirect_target = $redirect[0]->new_controller . "/" . $redirect[0]->new_slug;
                if (!empty($redirect_target)) {
                    $status_code = ($this->config->item("redirect_status_code")) ? $this->config->item("redirect_status_code") : 302;
                    if (!stristr($redirect_target, "?")) { # wenn get-parameter bisher nicht angehängt wurden
                        foreach ($_GET as $k => $v) {
                            $get_url_append_parts[] = "$k=$v";
                        }
                        $get_url_append = (!empty(@$get_url_append_parts)) ? "?" . implode("&", $get_url_append_parts) : "";
                        if (stristr($redirect_target, "#")) {
                            $redirect_target_parts = explode("#", $redirect_target);
                            $redirect_target = $redirect_target_parts[0] . $get_url_append . "#" . $redirect_target_parts[1];
                        } else {
                            $redirect_target = $redirect_target . $get_url_append;
                        }
                    }
                    redirect($redirect_target, 'auto', $status_code);
                }

            }
            if ($no_redirect_found) {
                show_404();
            }
        }
    }

    /**
     * Lädt die Unterseite "Top 50"
     */
    public function top50()
    {
        $this->smr_breadcrumbs->push('Top 50', 'top50');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Top 50 Reisepakete - unsere Bestseller!';
        $data['site_description'] = 'Top 50 Bestseller Reisepakete ✔ inkl. vielen Extras ✔ alle Hotels persönlich getestet ✔ jetzt günstig buchen!';

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'top50/top50_view' => $data,
            'common/footer' => []
        ));
    }

    /**
     * Gibt die Top 50 aus dem angegebenen JSON-File aus
     * todo: Top 50 über API fetchen
     * @param $list_type
     */
    public function getTop50($list_type = 'top-50-weekly')
    {
        $this->data = array();

        $api_data_string = $this->api_lib()->api_return(['get_top_list/', $list_type]);
        $api_data = json_decode($api_data_string, true);

        foreach ($api_data['top_list']['top_list_data'] as $list) {
            $this->data['list_type'] = $list_type;
            $this->data['top50Data'] = $list;
            $output[] = $this->get_view('top50/top50-package_view');
            $data['html']['append']['#top50-list'] = implode('', $output);
        }
        $this->show_ajax($data);
    }


    /**
     * Gibt die Preiskracher aus dem angegebenen JSON-File aus
     * @param $list_type
     */
    public function getPreiskracher($list_type = '2-nights-breakfast')
    {
        $this->data = array();

        $api_data_string = $this->api_lib()->api_return(['get_preiskracher_list', $list_type]);
        $api_data = json_decode($api_data_string, true);

        foreach ($api_data['preiskracher_list']['list_data'] as $list) {
            #$this->data['list_type'] = $list_type;
            $this->data['preiskracher_package'] = $list;
            $output[] = $this->get_view('preiskracher/preiskracher-package_view');
            $data['html']['append']['#preiskracher-list'] = implode('', $output);
        }
        $this->show_ajax($data);
    }

    /**
     * Lädt die Unterseite "Katalog"
     */
    public function katalog() {
        $this->smr_breadcrumbs->push('Katalog', 'katalog');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Reisekatalog von Spar mit! Reisen - kostenlos bestellen!';
        $data['site_description'] = 'Reisekatalog von Spar mit! Reisen ✔ über 390 Top-Reiseangebote ✔ kostenlose Lieferung ✔ jetzt bestellen!';

        if (!empty(array_filter($_POST))) {
            $formData = $_POST;
        } else if (!empty(array_filter($_GET))) {
            $formData = $_GET;
        }

        if (empty(array_filter($_POST)) && empty(array_filter($_GET))) {
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'katalog/katalog_view' => $data,
                'common/footer' => []
            ));
        } else {
            if (empty(array_filter($_GET))) {
                $this->load_templates(array(
                    'common/header' => $data,
                    'common/sidebar' => $data,
                    'katalog/katalog-sent_view' => $data,
                    'common/footer' => []
                ));
            }

            if (isset($formData['address_city'])) {
                $this->sendmail_lib->sendMail('katalog_intern', $formData);
                $this->sendmail_lib->sendMail('katalog_extern', $formData);
            }

            if (isset($formData['newsletter']) && !empty($formData['newsletter']) && filter_var(@$formData['email'], FILTER_VALIDATE_EMAIL)) {
                $newsletterData['email'] = $formData['email'];
                $newsletterData['title'] = $formData['title'];
                $newsletterData['firstname'] = $formData['firstname'];
                $newsletterData['lastname'] = $formData['lastname'];
                $this->sendmail_lib->addNewsletter($newsletterData);
            }

            if (!empty(array_filter($_GET))) {
                $data['html']['hide']['.order-katalog'] = '';
                $data['html']['show']['.katalog-success'] = '';
                $this->show_ajax($data);
            }
        }
    }

    /**
     * Lädt die Unterseite "Kontakt"
     */
    public function kontakt()
    {
        $this->smr_breadcrumbs->push('Kontakt', 'kontakt');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Kontaktformular - Spar mit! Reisen';
        $data['site_description'] = 'Kontaktformular Spar mit! Reisen ✔ 365 Tage im Jahr erreichbar ✔ Deutschlands Reiseveranstalter Nr. 1';
        #$data['show_5min_callback'] = true;

        if (empty(array_filter($_POST))) {
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'kontakt/kontakt_view' => $data,
                'common/footer' => $data
            ));
        } else {
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'kontakt/kontakt-sent_view' => $data,
                'common/footer' => $data
            ));
            $this->sendmail_lib->sendMail('kontakt_intern', $_POST);
            $this->sendmail_lib->sendMail('kontakt_extern', $_POST);
            if (isset($_POST['newsletter']) && filter_var(@$_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $newsletterData['email'] = $_POST['email'];
                $newsletterData['title'] = $_POST['title'];
                $newsletterData['firstname'] = $_POST['firstname'];
                $newsletterData['lastname'] = $_POST['lastname'];
                $this->sendmail_lib->addNewsletter($newsletterData);
            }
        }
    }

    /**
     * Lädt die Unterseite "Über uns -> Über Spar mit! Reisen"
     */
    public function ueber_spar_mit_reisen()
    {
        $this->smr_breadcrumbs->push('Über uns', 'ueber-uns/ueber-spar-mit-reisen');
        $this->smr_breadcrumbs->push('Über Spar mit! Reisen', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Was ist Spar mit! Reisen?';
        $data['site_description'] = 'Alles über Spar mit! Reisen ✔ Warum wir individuelle Reisen so mögen ✔ Warum wir Geiz nicht geil finden';

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'ueber-uns/ueber-spar-mit-reisen_view' => $data,
            'common/footer' => []
        ));
    }

    /**
     * Lädt die Unterseite "Über uns -> Das Unternehmen"
     */
    public function das_unternehmen()
    {
        $this->smr_breadcrumbs->push('Über uns', 'ueber-uns/ueber-spar-mit-reisen');
        $this->smr_breadcrumbs->push('Das Unternehmen', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Das Unternehmen Spar mit! Reisen';
        $data['site_description'] = 'Spar mit! Reisen vertreibt ausschließlich eigene Reisepakete, exklusiv über Internet und Telefon.';

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'ueber-uns/das-unternehmen_view' => $data,
            'common/footer' => []
        ));
    }

    public function treuebonus()
    {
        $this->smr_breadcrumbs->push('Über uns', 'ueber-uns/ueber-spar-mit-reisen');
        $this->smr_breadcrumbs->push('Treuebonus', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Treuebonus - Spar mit! Reisen';
        $data['site_description'] = 'Der Treuebonus von Spar mit! Reisen - sparen Sie als teuer Kunde bares Geld bei Ihren Kurzurlaub-Reisen!';

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'ueber-uns/treuebonus_view' => $data,
            'common/footer' => []
        ));
    }

    /**
     * Lädt die Unterseite "Über uns -> Team"
     */
    public function team($department = null)
    {
        $this->smr_breadcrumbs->push('Über uns', 'ueber-uns/ueber-spar-mit-reisen');
        if ($department == '') {
            $this->smr_breadcrumbs->push('Team', ' ');
        } else {
            $this->smr_breadcrumbs->push('Team', 'ueber-uns/team');
            $this->smr_breadcrumbs->push($department, ' '); //TODO Richtiges Breadcrumb ausgeben
        }
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Das Team bei Spar mit! Reisen';
        $data['site_description'] = 'Alle Mitarbeiter bei Spar mit! Reisen ✔ Reiseproducer ✔ Service-Center ✔ Marketing ✔ Admi ✔ Buchhaltung';

        if ($department == null) {
            $content_template = 'ueber-uns/team_view';
        } else {
            if (file_exists(VIEWPATH . 'ueber-uns/team/team-' . $department . '_view.php')) {
                $content_template = 'ueber-uns/team/team-' . $department . '_view';
            } else {
                show_404();
            }
        }

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            $content_template => $data,
            'common/footer' => []
        ));
    }

    /**
     * Gibt die Mitarbeiter aus dem angegebenen JSON-File aus
     * todo: Mitarbeiter im Backend verwalten und JSON über API fetchen
     * @param $department
     */
    public function getEmployees($department)
    {
        $this->data = array();
//        $json_file = file_get_contents(APPPATH . '../assets/data/team.json');
//        $teamJSON = json_decode($json_file, true);

        $api_data_string = $this->api_lib()->api_return(['getStaticpagesData/', 'team']);
        $api_data = json_decode($api_data_string, true);


        foreach ($api_data as $mitarbeiter_data) {
            if ($mitarbeiter_data['subtype'] == $department) {
                $this->data['teamData'] = $mitarbeiter_data;
            }
        }

        $output = $this->get_view('ueber-uns/team/team-department_view');
        $data['html']['append']['#employees'] = $output;
        $this->show_ajax($data);
    }

    /**
     * Lädt die Unterseite "Über uns -> Sponsoring"
     */
    public function sponsoring()
    {
        $this->smr_breadcrumbs->push('Über uns', 'ueber-uns/ueber-spar-mit-reisen');
        $this->smr_breadcrumbs->push('Sponsoring', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Sponsoring - Spar mit! Reisen';
        $data['site_description'] = 'Sport-Sponsoring - Tourismus und Sport, wie passt denn das zusammen? Bei Spar mit! Reisen ganz hervorragend!';

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'ueber-uns/sponsoring_view' => $data,
            'common/footer' => []
        ));
    }

    /**
     * Lädt die Unterseite "Über uns -> Partnerprogramm"
     */
    public function partnerprogramm()
    {
        $this->smr_breadcrumbs->push('Über uns', 'ueber-uns/ueber-spar-mit-reisen');
        $this->smr_breadcrumbs->push('Partnerprogramm', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Partnerprogramm - Spar mit! Reisen';
        $data['site_description'] = 'Unsere Partnerprogramme auf einen Blick - Vergütungsmodelle vorgestellt';

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'ueber-uns/partnerprogramm_view' => $data,
            'common/footer' => []
        ));
    }


    /**
     * Lädt die Unterseite "Service -> Rückruf anfordern"
     */
    public function rueckruf()
    {
        $this->smr_breadcrumbs->push('Service', 'service/rueckruf');
        $this->smr_breadcrumbs->push('Rückruf anfordern', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Rückruf anfordern - Spar mit! Reisen';
        $data['site_description'] = 'Unsere Reiseberater rufen Sie gern zwischen 8.00 und 20.00 Uhr zurück - jetzt Rückruf anfordern!';
        #$data['show_5min_callback'] = true;

        if (empty(array_filter($_POST))) {
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'service/rueckruf_view' => $data,
                'common/footer' => $data
            ));
        } else {
            $data['phone_prefix'] = $_POST['phone_prefix'];
            $data['phone_number'] = $_POST['phone_number'];
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'service/rueckruf-sent_view' => $data,
                'common/footer' => $data
            ));
            $this->sendmail_lib->sendMail('callback_intern', $_POST);
        }
    }

    /**
     * Lädt die Unterseite "Service -> Rückruf anfordern"
     */
    public function reisegutschein() {


            $midofficeData = array(
                'template_id' => $_POST['template'],
                'email' => $_POST['email'],
                'details' => array(
                    'gender' => $_POST['title'],
                    'lastname' => $_POST['lastname'],
                    'forename' => $_POST['firstname'],
                    'street' => $_POST['address_street'] . ' ' . $_POST['address_number'],
                    'city' => $_POST['address_city'],
                    'country' => $_POST['country'],
                    'phone' => @$_POST['phone_prefix'] . @$_POST['phone_number'],
                    'email' => $_POST['email'],
                    'comment' => @$_POST['comment'],
                    'sum' => $_POST['amount'],
                    'sender' => $_POST['from_name'],
                    'recipient' => $_POST['for_name'],
                    'voucherText' => @$_POST['voucher_text'],
                    'zip' => $_POST['address_plz']
                )
            );

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://midoffice.spar-mit.com/coupon/website/code.json;create",
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => http_build_query($midofficeData),
                CURLOPT_HTTPHEADER => array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8'),
            ));
            $result = curl_exec($curl);
            curl_close($curl);

            $this->sendmail_lib->sendMail('reisegutschein_intern', $_POST);
            if (isset($_POST['newsletter']) && filter_var(@$_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $newsletterData['email'] = $_POST['email'];
                $newsletterData['title'] = $_POST['title'];
                $newsletterData['firstname'] = $_POST['firstname'];
                $newsletterData['lastname'] = $_POST['lastname'];
                $this->sendmail_lib->addNewsletter($newsletterData);
            }
    }

    /**
     * Gibt Gutscheine der ausgewählten $category aus
     * @param $category
     */
    public function getVouchers($type = 'diverse') {

        $api_data_string = $this->fake_api_lib()->api_return(['getStaticpagesData', 'reisegutschein'], [], []);
        $api_data = json_decode($api_data_string, true);

        foreach ($api_data as $staticpage_data) {
            if (strtolower($staticpage_data['subtype']) == strtolower($type)) {
            foreach ($staticpage_data['content'] as $content) {
                $vouchers['category'] = $content['title'];

                foreach ($content['element'] as $element) {
                    if ($element['element_type_key'] == 'text') {
                        $vouchers['template_id'] = $element['text'];
                    }
                    if ($element['element_type_key'] == 'old_picture') {
                        $vouchers['path_thumb'] = $element['picture_items'][0]['url'];
                    }
                }

                if (@$vouchers['category'] && @$vouchers['path_thumb'] && @$vouchers['template_id']) {
                    $this->data['voucherData'] = $vouchers;
                    $output[] = $this->get_view('reisegutschein/vouchers_view');
                    $data['html']['append']['#vouchers'] = implode('', $output);
                }
                }
            }
        }
        $this->show_ajax($data);
    }

    /**
     * Lädt die Unterseite "Service -> Newsletter"
     */
    public function newsletter()
    {
        $this->smr_breadcrumbs->push('Service', 'service/newsletter');
        $this->smr_breadcrumbs->push('Newsletter', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Newsletter von Spar mit! Reisen';
        $data['site_description'] = 'Mit unserem kostenlosen Newsletter erhalten Sie alle 14 Tage interessante Angebote aus der Spar mit!-Urlaubswelt.';

        if (empty(array_filter($this->input->post()))) {
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'service/newsletter_view' => $data,
                'common/footer' => []
            ));
        } else {
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'service/newsletter-sent_view' => $data,
                'common/footer' => []
            ));
            if (isset($_POST['newsletter']) && filter_var(@$_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $this->sendmail_lib->addNewsletter($this->input->post());
            }
        }
    }

    public function newsletter_bestaetigung()
    {
        $this->smr_breadcrumbs->push('Service', 'service/newsletter_bestaetigung');
        $this->smr_breadcrumbs->push('Newsletter', 'Bestätigung');
        $this->smr_breadcrumbs->push('Bestätigung', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Newsletter von Spar mit! Reisen'; // TODO @marco
        $data['site_description'] = 'Mit unserem kostenlosen Newsletter erhalten Sie alle 14 Tage interessante Angebote aus der Spar mit!-Urlaubswelt.'; // TODO @marco

        if (empty(array_filter($this->input->post()))) {
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'service/newsletter_bestaetigung_view' => $data,
                'common/footer' => []
            ));
        }
    }


    public function newsletter_abmeldung()
    {
        $this->smr_breadcrumbs->push('Service', 'service/newsletter_abmeldung');
        $this->smr_breadcrumbs->push('Newsletter', 'Abmeldung');
        $this->smr_breadcrumbs->push('Abmeldung', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Newsletter von Spar mit! Reisen'; // TODO @marco
        $data['site_description'] = 'Mit unserem kostenlosen Newsletter erhalten Sie alle 14 Tage interessante Angebote aus der Spar mit!-Urlaubswelt.'; // TODO @marco

        if (empty(array_filter($this->input->post()))) {
            $data['email'] = null;
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'service/newsletter_abmeldung_view' => $data,
                'common/footer' => []
            ));
        } else {
            $data['email'] = $_POST['email'];
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'service/newsletter_abmeldung_view' => $data,
                'common/footer' => []
            ));
        }
    }

    /**
     * Lädt die Unterseite "Jobs"
     */
    public function jobs()
    {
        $this->smr_breadcrumbs->push('Jobs', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Jobs bei Spar mit! Reisen';
        $data['site_description'] = 'Jobs bei Spar mit! Reisen: wir suchen neue Mitarbeiterinnen und Mitarbeiter für unser Erfolgsteam.';

        $api_data_string = $this->api_lib()->api_return(['getStaticpagesData/', 'jobs']);
        $api_data = json_decode($api_data_string, true);

        foreach ($api_data as $job_data) {
            if ($job_data['subtype'] == "SMR") {
                $data['smr_jobs'] = $job_data;
            } else if ($job_data['subtype'] == "Fini-Resort") {
                $data['fini_jobs'] = $job_data;
            }
        }

//        $jobsJSON = file_get_contents(APPPATH . '../assets/data/jobs.json');
//        $jobsArray = json_decode($jobsJSON, true);
//
//        foreach ($jobsArray['sparmit'] as $sparmit_jobs) {
//            $data['jobs']['sparmit'][] = $sparmit_jobs;
//        }
//        foreach ($jobsArray['finiresort'] as $finiresort_jobs) {
//            $data['jobs']['finiresort'][] = $finiresort_jobs;
//        }

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'jobs/jobs_view' => $data,
            'common/footer' => []
        ));
    }


    /**
     * Lädt die Unterseite "Impressum"
     */
    public function impressum()
    {
        $this->smr_breadcrumbs->push('Impressum', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Impressum - Spar mit! Reisen';
        $data['site_description'] = 'Spar mit! Reisen, Mattenstraße 24, CH-4058 Basel, Geschäftsführender Inhaber: Mathias Finck';

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'impressum/impressum_view' => $data,
            'common/footer' => []
        ));
    }


    /**
     * Lädt die Unterseite "AGBs"
     */
    public function agb()
    {
        $this->smr_breadcrumbs->push('AGBs', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'AGBs - Spar mit! Reisen';
        $data['site_description'] = 'Die Buchung der Reiseleistungen von Spar mit! Reisen erfolgt auf Grundlage der allgemeinen Reisebedingungen.';

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'agb/agb_view' => $data,
            'common/footer' => []
        ));
    }


    /**
     * Lädt die Unterseite "Datenschutz"
     */
    public function datenschutz()
    {
        $this->smr_breadcrumbs->push('Datenschutz', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Datenschutz - Spar mit! Reisen';
        $data['site_description'] = 'Das Unternehmen Spar mit! Reisen nimmt den Schutz Ihrer persönlichen Daten sehr ernst und hält sich strikt an die Regeln der Datenschutzgesetze.';

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'datenschutz/datenschutz_view' => $data,
            'common/footer' => []
        ));
    }


    /**
     * Lädt die Fehlerseite 404"
     */
    public function error404()
    {
        # check for redirect in db
        if (1) {
            $uri_string = $this->router->uri->uri_string;
            $url_segments = $this->uri->segment_array();
            $redirect = $this->redirect_api_model()->check_redirect_url($uri_string);
            $status_code = ($this->config->item("redirect_status_code"))? $this->config->item("redirect_status_code") : 302;
            if (is_object($redirect)) {
                $target = $redirect->new_controller . "/" . $redirect->new_slug;
            } elseif (is_array($redirect) && !empty($redirect)) {#multiple matches - should not occure!
                $target = $redirect[0]->new_controller . "/" . $redirect[0]->new_slug;
            } else
                $no_redirect_found = true;
            if (!@$no_redirect_found && @$target) {
                if (!stristr($target, "?")) { # wenn get-parameter bisher nicht angehängt wurden
                    foreach ($_GET as $k => $v) {
                        $get_url_append_parts[] = "$k=$v";
                    }
                    $get_url_append = (!empty(@$get_url_append_parts)) ? "?" . implode("&", $get_url_append_parts) : "";
                    if (stristr($target, "#")) {
                        $target_parts = explode("#", $target);
                        $target = $target_parts[0] . $get_url_append . "#" . $target_parts[1];
                    } else {
                        $target = $target . $get_url_append;
                    }
                }
                redirect($target, 'auto', $status_code);
            }
        }
        if ($no_redirect_found) {
            if (end($url_segments) == "termine.php") {
                # like https://www.spar-mit.com/badenweiler1/termine.php
                redirect("paket/".$url_segments[count($url_segments) - 1]."#termine");
            } else {
                show_404();
            }
        }
    }

    /**
     * Lädt die Unterseite Auswaertsspiel"
     */
    public function auswaertssieg($department = null)
    {
        $this->smr_breadcrumbs->push('REISEPLANER AUSWAERTSSIEG', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Bestellung "REISEPLANER AUSWÄRTSSIEG"';
        $data['site_description'] = 'Bestellung "REISEPLANER AUSWÄRTSSIEG"';

        if (empty(array_filter($_POST))) {
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'auswaertsspiel/auswaerts_view' => $data,
                'common/footer' => []
            ));
        } else {
            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'auswaertsspiel/auswaerts-sent_view' => $data,
                'common/footer' => []
            ));
            $midofficeData = array(
                'email' => $_POST['email'],
                'details' => array(
                    'gender' => $_POST['title'],
                    'lastname' => $_POST['lastname'],
                    'forename' => $_POST['firstname'],
                    'street' => $_POST['address_street'] . ' ' . $_POST['address_number'],
                    'city' => $_POST['address_city'],
                    'country' => $_POST['country'],
                    'phone' => @$_POST['phone_prefix'] . @$_POST['phone_number'],
                    'email' => $_POST['email'],
                    'zip' => $_POST['address_plz']
                )
            );
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://midoffice.spar-mit.com/coupon/website/code.json;create",
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => http_build_query($midofficeData),
                CURLOPT_HTTPHEADER => array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8'),
            ));
            $result = curl_exec($curl);
            curl_close($curl);
            $this->sendmail_lib->sendMail('auswaertsieg_intern', $_POST);
            $this->sendmail_lib->sendMail('auswaertsieg_extern', $_POST);
            if (isset($_POST['newsletter']) && filter_var(@$_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $newsletterData['email'] = $_POST['email'];
                $newsletterData['title'] = $_POST['title'];
                $newsletterData['firstname'] = $_POST['firstname'];
                $newsletterData['lastname'] = $_POST['lastname'];
                $this->sendmail_lib->addNewsletter($newsletterData);
            }
        }
    }

    /**
     * Lädt die Unterseite "Kartensuche"
     */
    public function kartensuche($country = null)
    {
        if ($country == null) {
            $this->smr_breadcrumbs->push('Kartensuche', 'kartensuche');
            $this->smr_breadcrumbs->push('Kartensuche', ' ');
            $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

            $data['site_title'] = 'Kartensuche - Spar mit! Reisen';
            $data['site_description'] = 'Ganz schnell zum gewünschten Reisepaket mit der Kartensuche von Spar mit! Reisen.';

            $api_data_string = $this->api_lib()->api_return(['gettopic/reiseziele/reiseziele']);
            $api_data = json_decode($api_data_string, true);

            $data['reiseziele'] = $api_data;

            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'map/mapsearch_view' => $data,
                'common/footer' => []
            ));

        } else if($country) {
            $this->smr_breadcrumbs->push('Kartensuche', 'kartensuche');
            $this->smr_breadcrumbs->push(ucfirst($country), ' ');
            $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

            $topic_data_string = $this->api_lib()->api_return(['gettopic/reiseziele/' . $country]);
            $topic_data = json_decode($topic_data_string, true);

            $api_data_string = $this->api_lib()->api_return(['getTopicCoordinates', $topic_data['topic']['id']]);
            $data['coordinates'] = json_decode($api_data_string, true);

            $data['site_title'] = $topic_data['topic']['title'] . ' - Kartensuche - Spar mit! Reisen';
            $data['site_description'] = 'Ganz schnell zum gewünschten Reisepaket in ' . $topic_data['topic']['title'] . ' mit der Kartensuche von Spar mit! Reisen.';

            $this->load_templates(array(
                'common/header' => $data,
                'common/sidebar' => $data,
                'map/map-countries_view' => $data,
                'common/footer' => []
            ));
        }
    }


    public function mustache()
    {
        $this->smr_breadcrumbs->push('Deutschland', 'deutschland');
        $this->smr_breadcrumbs->push('Deutschland', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Deutschland - Kartensuche - Spar mit! Rei sen';
        $data['site_description'] = 'Ganz schnell zum gewünschten Reisepaket in Deutschland mit der Kartensuche von Spar mit! Reisen.';

        $this->load_templates(array(
            'mustache/packagestripe.mustache' => $data
        ));
    }

    /**
     * Lädt die Unterseite "Impressum"
     */
    public function bookmark()
    {
        $this->smr_breadcrumbs->push('Merkliste', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Merkliste - Spar mit! Reisen';
        $data['site_description'] = 'Spar mit! Reisen, Mattenstraße 24, CH-4058 Basel, Geschäftsführender Inhaber: Mathias Finck';

        foreach ($_COOKIE as $key => $cookie) {
                if ($cookie == "bookmark") {
                    $package_number = explode("package_", $key);
                    $bookmarked_packages[] = $package_number[1];
                }
        }

        $data['bookmarked_packages'] = @$bookmarked_packages;

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'common/bookmark_view' => $data,
            'common/footer' => []
        ));
    }

    /**
     * Lädt die Unterseite "Impressum"
     */
    public function videos()
    {
        $this->smr_breadcrumbs->push('Videos', ' ');
        $data['breadcrumbs'] = $this->smr_breadcrumbs->show();

        $data['site_title'] = 'Videos - Spar mit! Reisen';
        $data['site_description'] = 'Videos von Spar mit! Reisen, Mattenstraße 24, CH-4058 Basel, Geschäftsführender Inhaber: Mathias Finck';

        $this->load_templates(array(
            'common/header' => $data,
            'common/sidebar' => $data,
            'videos/videos_view' => $data,
            'common/footer' => []
        ));
    }



}
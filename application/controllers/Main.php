<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller
{
    public $helpers = array('url');
    public $libraries = array('curl_lib', 'smr_breadcrumbs', 'sendmail_lib', 'packagestripe_lib');

    /** @var  Curl_lib $curl_lib */
    public $curl_lib;
    /** @var  Packagestripe_lib $packagestripe_lib */
    public $packagestripe_lib;


    public function index() {
        $this->load->library('media_lib');
        $topic = $this->uri->segment(1);
        $topic_sub = $this->uri->segment(2);

        if (is_null($topic)) {
            $topic = 'home';
            $view = 'home/index_view';
        }

        if ($this->input->get_post("form_data")) {
            $formData = $this->input->get_post("form_data");
            $view = 'staticpage/form-danke_view';
            $this->sendmail_lib->sendMail($topic . '_intern', $formData);
        }

        $data['topic'] = $topic;

        $api_response_object = $this->fake_api_lib()->api_return(['getTopic', $topic, $topic_sub], [], []);
        $api_data = $api_response_object->get_decoded();

        if (!@$view) {
            if ($topic == "autoren-termine" && $topic_sub) {
                $view = 'staticpage/autor_view';
            } else {
                $view = 'staticpage/' . $api_data['topic']['class'] . '_view';
                if (!file_exists(APPPATH.'views/'.$view.'.php')) {
                    $view = 'staticpage/standard_view';
                } 
            }
        }

        $data['data'] = $api_data;

        $this->load_templates(array(
            'common/header' => $data,
             $view => $data,
            'common/footer' => $data
        ));
    }


    /**
     * Test-Ansicht für E-Mail Template
     */
    public function mail() {
        $this->load_templates(array(
            'mail/mail_view' => []
        ));
    }
    /**
     * Sende Testmail
     */
    public function test_mail() {
        $formdata['title'] = 'Herr';
        ci()->load->library('sendmail_lib');
        ci()->sendmail_lib->sendMail('test', $formdata);
    }

    /**
    * Methode API leitet die API anfragen direct zu  http://backend.sparmit.local/public/api/
    * weiter und umgeht somit X-ORIGIN Exeptions im Browser
    *
    * @param STRING $v1 erste Variable
    * @param STRING $v2 zweite Variable
    * @param STRING $v3 dritte Variable
    *
    * @return STRING
    */
    public function api($v1 = NULL, $v2 = NULL, $v3 = NULL) {
        $pattern = "/";
        if(!empty($v1)){
            $pattern .= $v1."/";
        }
        if(!empty($v2)){
            $pattern .= $v2."/";
        }
        if(!empty($v3)){
            $pattern .= $v3."/";
        }
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, "http://$this->api_server/public/api".$pattern);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        $output = curl_exec($ch);
//        curl_close($ch);
//        echo $output;
        echo $this->curl_lib->get_api_data("/public/api".$pattern);
    }


    /**
     *  Gibt das komplette Object der Tabelle als JSON wieder
     *  Wenn der Paramenter einem existierenden Model entspricht,
     *  wird dort die jeweilige get_ methode angewendet.
     *
     * Bsp:
     * /json/news
     *
     * @param string $table
     * @return mixed
     */
    public function json($table = '') {
        $dir = './application/models/';
        $files = scandir($dir);
        $file = ucfirst($table) . '_model.php';

        if (in_array($file, $files)) {
            $model_name = $table . '_model';
            $methode = 'get_' . $table;
            if (method_exists($model_name, $methode)) {
                $data['json'] = $this->$model_name->$methode();
                echo json_encode($data['json']);
            } else {
                //echo "methode existiert nicht";
            }
        } else {
            show_404();
        }
    }


}

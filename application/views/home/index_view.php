<div class="row">

    <div class="col-sm-12 row home_pictures">
        <div class="col-sm-6">
            <div class="gallery-wrap">
                <figure>
            <a href="#video" style="cursor: pointer">
                <img src="<?= site_url('assets/img/pictures/linda-schober.png') ?>">
                <span class="count">Video</span>
            </a>
                </figure>
            </div>
        </div>
        <div class="col-sm-6">
            <?= ci()->get_view('common/element_types', array("element" => @$data['topic']['gallery'])); ?>
<!--            <img src="--><?//= site_url('assets/img/pictures/badenweiler.png') ?><!--">-->
        </div>
    </div>

    <?php foreach ($data['topic']['children'] as $link) {
      echo ci()->get_view('common/link-box_view', ['link' => $link]);
    } ?>
</div>


<div class="picture" id="video">
<?php
if (@$data['topic']['content']) {
    foreach ($data['topic']['content'] as $content) {
        if ($content['content_type_key'] == 'infotext') {
            foreach ($content['element_items'] as $element) {
                echo ci()->get_view('common/element_types', array("element" => $element));
            }
        }
    }
}
    if (@$data['topic']['picture']) {
        if (@ci()->mobile_detect_lib->is_mobile()) {
            $max_picture_width = 512;
        } else {
            $max_picture_width = 800;
        }
        foreach ($data['topic']['picture'] as $picture) { ?>
            <img src="<?= Media_lib::url(@$picture, $max_picture_width) ?>">
    <?php } } ?>
</div>
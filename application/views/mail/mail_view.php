<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>E-Mail Template</title>
    <style type="text/css">
        body{width:100% !important; font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; color: #333333;}
        .imagefix {display:block;}
        img {max-width: 100%; outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
        a img {border:none;}
        p {margin: 0 0 .5rem 0;}
        h1, h2, h3, h4, h5, h6 {color: #333333 !important;}
        h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: #666666 !important;}
        h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active { color: #666666 !important; }
        h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited { color: #666666 !important; }
        table td {border-collapse: collapse;}
        table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
        a { color: #666666; text-decoration: none; }
        a:hover { color: #C00D0D; }
        p.spacer { height: 16px; }
        td.spacer { height: 1rem; }
        hr { margin-top: 1rem; margin-bottom: 1rem; border: 0; height: 0; border-top: 1px dashed #5D96CB; border-bottom: 1px dashed #fefefe; }
        .nowrap { white-space: nowrap; }

        .main-table {
            margin: 0;
            padding: 0;
        }

        .header-table {
            background-color: #C00D0D;
        }
        .header-table .header-logo-wrap {
            padding-left: .5rem;
        }
        .header-table .header-logo-wrap .header-logo {
            margin-top: .5rem;
            margin-bottom: .5rem;
        }
        .header-table .header-text-wrap {
            padding-right: .5rem;
            font-size: 1.1rem;
        }
        .header-table .header-text-wrap .header-text {
            color: #fefefe;
        }

        .headerlinks-table {
            margin-top: .5rem;
            margin-bottom: .5rem;
        }
        .headerlinks-table .headerlinks-td {
            display: inline-block;
            padding-right: 1rem;
        }
        .headerlinks-table .headerlink {
            font-size: .8rem;
            font-weight: 700;
            color: #333333;
        }
        .headerlinks-table .headerlink:hover {
            color: #C00D0D;
        }

        .content-table {

        }
        .content-grey {
            background-color: #f5f5f5;
            padding: 1rem;
        }
        .content-table .content-wrap {
            padding: 1rem 2rem;
        }

        .footer-table {
            margin-top: 2rem;
            background-color: #EAF2F5;
        }
        .footer-table .service-center-wrap {

        }
        .footer-table .service-center-wrap .service-center-big {
            display: block;
            width: 100%;
        }
        .footer-table .social-media-wrap tr td {
            padding-left: .5rem;
            padding-right: .5rem;
        }
        .footer-table .social-media-wrap .social-media-title {
            display: block;
            margin-bottom: .25rem;
            font-size: .6rem;
        }
        .footer-table .social-media-wrap .social-media-facebook {
            color: #3A5999;
        }
        .footer-table .social-media-wrap .social-media-googleplus {
            color: #DC4B36;
        }
        .footer-table .social-media-wrap .social-media-youtube {
            color: #E5221C;
        }
        .footer-table .social-media-wrap .social-media-twitter {
            color: #578FCA;
        }
        .footer-table .impressum-wrap .impressum-logo {

        }
        .footer-table .impressum-wrap .impressum-text {
            font-size: .8rem;
        }

        @media only screen and (max-width: 360px) {
            .hidden-xs {
                display: none !important;
            }
            .header-table .header-logo-wrap .header-logo {
                width: 100px;
            }
            .header-table .header-text-wrap .header-text {
                font-size: .9rem;
            }
            .content-table .content-wrap {
                padding: 1rem;
            }
            .footer-table .impressum-wrap .impressum-text {
                padding-left: .5rem;
                padding-right: .5rem;
                font-size: .7rem;
            }
        }

        @media only screen and (min-width: 361px) and (max-width: 768px) {
            .hidden-sm {
                display: none !important;
            }
            .main-table {
                width: 100%;
            }
            .footer-table .impressum-wrap .impressum-text {
                padding-left: .5rem;
                padding-right: .5rem;
            }
        }

        @media only screen and (min-width: 769px) {
            body {
                margin-top: 1.5rem;
                margin-bottom: 1.5rem;
                background-color: #eeeeee;
            }
            .main-table {
                margin: auto;
                width: 700px;
                background-color: #fefefe;
            }
        }
    </style>
</head>
<body>

    <table cellpadding="0" cellspacing="0" border="0" align="center" class="main-table">
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="header-table">
                    <tr>
                        <td valign="middle" class="header-wrap">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="header-logo-wrap">
                                        <a href="https://www.spar-mit.com"><img src="<?= site_url('assets/img/mail/smr.png') ?>" alt="Spar mit! Reisen" class="imagefix header-logo"></a>
                                    </td>
                                    <td align="right" class="header-text-wrap">
                                        <span class="header-text"><?= @$mailData['header'] ?></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="content-table">
                    <tr>
                        <td class="spacer" height="1rem"></td>
                    </tr>
                    <tr>
                        <td class="content-wrap">
                            <?= @$mailData['content'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="spacer" height="1rem"></td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="footer-table">
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="social-media-wrap">
                                <tr>
                                    <td align="center">
                                        <a href="https://www.facebook.com/sparmitreisen" class="social-media-facebook">
                                            <img src="<?= site_url('assets/img/mail/facebook.jpg') ?>" alt="Folgen Sie uns auf Facebook" class="imagefix">
                                        </a>
                                    </td>
                                    <td align="center">
                                        <a href="https://plus.google.com/+sparmitreisen" class="social-media-googleplus">
                                            <img src="<?= site_url('assets/img/mail/googleplus.jpg') ?>" alt="Seien Sie dabei mit google+" class="imagefix">
                                        </a>
                                    </td>
                                    <td align="center">
                                        <a href="https://www.youtube.com/channel/UCeZksYDJhCLqAVpPuI3a87w" class="social-media-youtube">
                                            <img src="<?= site_url('assets/img/mail/youtube.jpg') ?>" alt="Besuchen Sie uns auf YouTube" class="imagefix">
                                        </a>
                                    </td>
                                    <td align="center">
                                        <a href="https://twitter.com/SparMitReisen" class="social-media-twitter">
                                            <img src="<?= site_url('assets/img/mail/twitter.jpg') ?>" alt="Folgen Sie uns auf twitter" class="imagefix">
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="impressum-wrap">
                                <tr>
                                    <td valign="top" align="center" class="impressum-logo hidden-xs hidden-sm">
                                        <a href="https://www.spar-mit.com">
                                            <img src="<?= site_url('assets/img/mail/smr.png') ?>" alt="Spar mit! Reisen">
                                        </a>
                                    </td>
                                    <td valign="top">
                                        <p class="impressum-text">
                                            Mattenstrasse 24 | CH-4058 Basel<br>
                                            <span class="nowrap">Telefon (+49) 7621 - 91 40 111</span> | <span class="nowrap">Telefax (+49) 7621 - 91 40 112</span><br>
                                            www.spar-mit.com | kontakt@spar-mit.com
                                        </p>
                                        <p class="impressum-text">
                                            Herausgeber: <strong>Spar mit! Reisen</strong>, Mathias Finck<br>
                                            Geschäftsführender Inhaber: Mathias Finck<br>
                                            Handelsregister Kanton Basel-Stadt: CH-270.1.012.887.9
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</body>
</html>
<div class="element-wrap <?= $this->packagestripe_lib->css_viewport($element) ?> <?= (@$element['element_type_key']); ?>">
    <?php if ($element['element_type_key'] == 'text'): ?>
        <p><?= @$element['text'] ?></p>
    <?php endif; ?>

    <?php if ($element['element_type_key'] == 'subheading'): ?>
        <?php if (@$element['type_id'] == 1): ?>

            <span class="subtitle-big"><?= @$element['text'] ?></span>
        <?php else: ?>
            <h3><?= @$element['text'] ?></h3>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($element['element_type_key'] == 'subheadingsmall'): ?>
        <span class="subtitle-small"><?= @$element['text'] ?></span>
    <?php endif; ?>

    <?php if ($element['element_type_key'] == 'text_ck' || $element['element_type_key'] == 'infobox'): ?>
        <?= @$element['text'] ?>
    <?php endif; ?>

    <?php if ($element['element_type_key'] == 'package_price'): ?>

        <div class="pricelabel-foldpage hidden-sm hidden-mds">
            <div class="row">
                <div class="row col-sm">
                    <div class="subtitle">Spar mit!-Preis</div>
                    <div class="unit">&nbsp;<?= $element['text'] ?></div>
                </div>
                <div class="row col-sm end-sm">
                    <div class="duration"><?= $element['package_price_duration'] ?></div>
                    <div class="value end-sm">&nbsp;EUR <?= $element['package_price'] ?>,-</div>
                </div>
            </div>
            <hr>
        </div>

        <div class="pricelabel-foldpage hidden-lg hidden-mdl">
            <span class="subtitle">Spar mit!-Preis</span>
            <div class="pricelabel-foldpage">
                <div class="row">
                    <div class="col-sm">
                        <div class="duration"><?= $element['package_price_duration'] ?></div>
                        <div class="unit"><?= $element['text'] ?></div>
                    </div>
                    <div class="col-sm">
                        <div class="value end-sm">EUR <?= $element['package_price'] ?>,-</div>
                    </div>
                </div>
            </div>
            <hr>
        </div>

    <?php endif; ?>

    <?php if ($element['element_type_key'] == 'video'): ?>
        <figure class="video-modal-wrap">
            <img src="<?= @$element['picture_items'][0]['url'] ?>" alt="Video abspielen">
            <a href="<?= @$element['text'] ?>" class="play-icon-wrap" data-modal-type="iframe">
                <div class="play-icon">
                    <i class="fa fa-youtube-play"></i>
                </div>
            </a>
        </figure>
    <?php endif; ?>

    <?php if ($element['element_type_key'] == 'hotelmap'): ?>
        <?php if ($element['lat'] != 0 && $element['lng'] != 0 && !empty($element['title'])) { ?>
            <?= ci()->get_view("map/map-paket_view.php", ['mapinfos' => $element]) ?>
        <?php } ?>
    <?php endif; ?>

    <?php if ($element['element_type_key'] == 'inline_video'): ?>
        <?php
        $url = urldecode(rawurldecode(@$element['text']));
        preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
        $youtube_video_id = @$matches[1];

        if ($youtube_video_id)
            $video_src = "https://www.youtube.com/embed/$youtube_video_id?rel=0&amp;showinfo=0";
        else {
            $video_src = $url;
        }
        ?>
        <div class="inline-video-wrap">
            <iframe src="<?= $video_src ?>" class="inline-video" frameborder="0" allowfullscreen></iframe>
        </div>
    <?php endif; ?>

    <?php if ($element['element_type_key'] == 'old_picture' ||$element['element_type_key'] == 'picture'): ?>
        <?php
        if (@ci()->mobile_detect_lib->is_mobile()) {
        $max_picture_width = 512;
        } else {
        $max_picture_width = 800;
        }
        ?>
        <div class="gallery-wrap">
            <figure>
                <img src="<?= Media_lib::url(@$element['picture_item'], $max_picture_width) ?>">

            </figure>
        </div>
    <?php endif; ?>

    <?php if ($element['element_type_key'] == 'gallery' && @$element['gallery_item']): ?>

    <div class="gallery-wrap is-not-inited" data-gallery-id="<?= @$element['element_id'] ?>">
        <figure style="overflow: hidden;">
            <?php
            $gallery_items = $element['gallery_item'];
            if (@ci()->mobile_detect_lib->is_mobile()) {
                $max_picture_width = 512;
            } else {
                $max_picture_width = 800;
            }
            $imgIndex = 0;
            echo '<div class="row arrows">';
                echo '<span class="col-sm start-sm arrow-left"><i class="fa fa-chevron-left"></i></span>';
                echo '<span class="col-sm end-sm arrow-right"><i class="fa fa-chevron-right"></i></span>';
                echo '</div>';
            echo '<div class="carousel is-not-inited" data-slick=\'{"arrows": false, "lazyLoad": "ondemand", "zindex": 2, "adaptiveHeight": true, "easing": "easeOutCubic"}\'>';
            foreach ($gallery_items['picture_items'] as $picture_item) {
            echo '<div class="carousel-inner"><img data-lazy="' . Media_lib::url($picture_item, $max_picture_width) . '" alt="' . @$picture_item['title'] . '"></div>';
            }
            echo '</div><span class="count">' . count($gallery_items['picture_items']) . ' Fotos</span>';
            ?>
        </figure>
        <div class="gallery-images hidden">
                <?php foreach ($gallery_items['picture_items'] as $picture_item): ?>
                    <?php if (empty($picture_item['path'])) {
                        $picture_item_width = $picture_item['width'];
                    } else if (@ci()->mobile_detect_lib->is_mobile()) {
                        $picture_item_width = 512;
                    } else {
                        $picture_item_width = Media_lib::get_valid_width(@$picture_item['width']);
                    }
                    ?>
                    <div data-path="<?= Media_lib::url($picture_item, $picture_item_width, true) ?>"
                         data-title="<?= html_escape(@$picture_item['text']) ?>"
                         data-height="<?= Media_lib::get_ratio_height($picture_item, $picture_item_width) ?>"
                         data-width="<?= $picture_item_width ?>"></div>
                <?php endforeach; ?>
            <script>
                $(function () {
                    slick_init_carousel();
                    photoswipe_init_falte();
                });
            </script>
        </div>
    </div>
    <?php endif; ?>
</div>
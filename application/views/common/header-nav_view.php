<div class="row hidden-sm">
    <a href="<?= site_url() ?>" class="navi-link">
       Startseite
    </a>
    <?php foreach ($header_links as $link) {
        if (strpos($link['url'], 'www.') !== false) {
            $url = $link['url'];
        } else {
            $url = site_url($link['url']);
        } ?>
        <a href="<?= $url ?>" class="navi-link">
            <?= $link['title'] ?>
        </a>
    <?php } ?>
</div>
<div class="hidden-lg hidden-md">
    <nav class="end-sm burger">
        <button class="btn" onclick="toggleMobileMenu()"><i class="fa fa-bars"></i></button>
    </nav>
</div>
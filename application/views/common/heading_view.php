<?php
    if (@$class !== "seiten")
        $bildlink = "assets/img/icons/buch.png";
    else
        $bildlink = "assets/img/icons/buch-blau.png"
?>

<h1 class="heading <?= @$class ?>">
    <img src="<?= site_url($bildlink) ?>"> <?= $title ?>
</h1>
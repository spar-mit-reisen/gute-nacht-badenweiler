<ul class="mobile-menu">
    <li>
        <a href="<?= site_url('') ?>" class="navi-link">
           Startseite
        </a>
    </li>
<?php foreach (@$header_links as $link) { ?>
    <li>
        <a href="<?= site_url($link['url']) ?>" class="navi-link">
            <?= $link['title'] ?>
        </a>
    </li>
<?php } ?>
    <li></li>
    <?php foreach (@$footer_links as $link) { ?>
        <li>
            <a href="<?= site_url($link['url']) ?>" class="navi-link">
                <?= $link['title'] ?>
            </a>
        </li>
    <?php } ?>
</ul>

<?php
if (!@$date['url'])
    $onclick = "onclick='return false;'";

?>

<div class="col-sm-12 termin-box <?php if(!@$date['picture']) { ?>incative<?php } ?>">
    <a href="<?= site_url($date['url']); ?>" <?= @$onclick ?>>
        <div class="row">
            <div class="col-sm-3 col-lg-2 autor-picture">
                <?php
                if (isset($date['picture'])) {
                    $max_picture_width = 128;
                    foreach ($date['picture'] as $picture) { ?>
                        <img src="<?= Media_lib::url(@$picture, @$max_picture_width) ?>">
                        <?php break; } } else { ?>
                    <img class="book" src="<?= site_url("assets/img/icons/buch.png") ?>">
             <?php } ?>
            </div>
            <div class="col-sm-9 col-lg-10 row autor-content">
                <div class="col-sm-12 date">
                    <?= date('j. n.', strtotime($date['event_start'])) ?> - <?= date('j. n. Y', strtotime($date['event_end'])) ?>
                </div>
                <div class="col-sm-12 name">
                    <?= $date['title'] ?>
                </div>
                <div class="col-sm-11 text">
                    <?= $date['subtitle'] ?>
                </div>
            </div>
        </div>
        <div class="inner-box"></div></a>
</div>



<!DOCTYPE html>
<html lang="de">
<head>
    <title>Gute Nacht, Badenweiler!</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Wellness und Literatur im Erlebnishotel Fini-Resort Badenweiler">
    <link href="<?= site_url('assets/img/logos/gute-nacht-quadrat.jpg')?>" rel="icon" type="image/png">
    <link href="<?= site_url('assets/css/main.min.css') ?>?v=<?= filemtime(FCPATH . 'assets/css/main.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" rel="stylesheet" type="text/css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>

</head>

<body>

<nav class="mobile-menu-wrap hidden-lg">
    <?= ci()->get_view('common/mobile-menu_view', ['header_links' => @$data['topic']['header_links'], 'footer_links' => @$data['topic']['footer_links']]); ?>
</nav>

    <div class="container box">
        <header>
            <div class="header-image">
                <a href="<?= site_url(); ?>">
                    <img src="<?= site_url('assets/img/header/gute-nacht-badenweiler.png')?>">
                </a>
            </div>
            <?php if (isset($data['topic']['header_links'])) { ?>
                <?= ci()->get_view('common/header-nav_view', ['header_links' => $data['topic']['header_links']]); ?>
            <?php } ?>
        </header>
        <div class="content">





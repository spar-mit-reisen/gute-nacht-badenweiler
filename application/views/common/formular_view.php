<form method="post">
    <div class="row">
    <?php
    foreach ($content['element_items'] as $element) {
        $search  = array('-', ' ', '+', '/');
        $replace = array('', '', '', '');
        $name = str_replace($search, $replace, $element['text']);

        if ($element['element_type_key'] == "input") {
            ?>
            <div class="col-sm-12">
                <input name="form_data[<?= $name ?>]" placeholder="<?= $element['text'] ?> *" required>
            </div>
    <?php } else if ($element['element_type_key'] == "textarea") { ?>
            <div class="col-sm-12">
                <textarea name="form_data[<?= $name ?>]" rows="8" placeholder="<?= $element['text'] ?> *" required></textarea>
            </div>
    <?php } }  ?>
        <div class="col-sm-12">
            <button>Senden</button>
        </div>
    </div>
</form>

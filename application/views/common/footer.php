<footer>
    <div class="row">
        <a class="items" href="https://spar-mit.com" target="_blank">
            <img src="<?= site_url('assets/img/logos/spar-mit-reisen.png')?>">
        </a>
        <a class="items" href="http://www.fini-resort-badenweiler.de" target="_blank">
            <img src="<?= site_url('assets/img/logos/fini-resort-badenweiler.png')?>">
        </a>
        <div class="items row social">
            <a href="https://www.facebook.com/gutenachtbadenweiler/" target="_blank">
                <img src="<?= site_url('assets/img/icons/facebook.png')?>">
            </a>
            <a href="https://www.youtube.com/channel/UCeZksYDJhCLqAVpPuI3a87w" target="_blank">
                <img src="<?= site_url('assets/img/icons/youtube.png')?>">
            </a>
        </div>
        </a>
        </a>
        <div class="items">
            <div class="footer-links">
                <div class="row">
                    <?php
                    foreach (@$data['topic']['footer_links'] as $footer_link) { ?>
                        <div class="col-sm col-lg-12"><a href="<?= site_url($footer_link['url'])?>"><?= $footer_link['title'] ?></a></div>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</footer>

</div>
</div>
<?= ci()->get_view('common/photoswipe_tpl', ''); ?>
<script src="<?= site_url('assets/js/main.min.js') ?>?v=<?= filemtime(FCPATH . 'assets/js/main.min.js') ?>"></script>
<script>
    $(function () {
        backToTopButton();
    });
</script>
</body>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120828375-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-120828375-1');
</script>
</html>
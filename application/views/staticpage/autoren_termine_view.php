<?= ci()->get_view('common/heading_view', ['title' => $data['topic']['title']]); ?>

<div class="row text">
    <?php foreach ($data['topic']['children'] as $date) {
        echo ci()->get_view('common/termine-box_view', ['date' => $date]);
    } ?>
</div>

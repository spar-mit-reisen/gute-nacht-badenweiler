<?= ci()->get_view('common/heading_view', ['title' => $data['topic']['title'], 'class' => $data['topic']['class']]); ?>

<div class="<?= $data['topic']['class'] ?>">
<?php
if (isset($data['topic']['content'])) {
    foreach ($data['topic']['content'] as $content) {
        foreach ($content['element_items'] as $element) {
            echo ci()->get_view('common/element_types', array("element" => $element));
            if (@$element['mobile'] != 1) {
                $show_more = true;
            }
        }

        if ($content['content_type_key'] == "formular") {
            echo ci()->get_view('common/formular_view', array("content" => $content));
        }
    }

    if (@$show_more) { ?>
        <div class="show_more">
        <button class="btn-clear-blue btn-sm show-full-foldpage hidden-lg" onclick="showFullFoldpage(event)">
            mehr...
        </button>
        </div>
 <?php }
}?>

<?php if (isset($data['topic']['picture'])) { ?>
<div class="picture">
    <?php
    if (@ci()->mobile_detect_lib->is_mobile()) {
    $max_picture_width = 512;
    } else {
    $max_picture_width = 800;
    }
    foreach ($data['topic']['picture'] as $picture) { ?>
    <img src="<?= Media_lib::url(@$picture, $max_picture_width) ?>">
    <?php } ?>
</div>
<?php } ?>
</div>

<?php foreach (@$data['topic']['content'] as $content) {
    if ($content['content_type_key'] == 'socialmedia') {
        $social_media = true;
        break;
    }
} ?>

<?= ci()->get_view('common/heading_view', ['title' => $data['topic']['title']]); ?>

<div class="row mobile-picture-first">
    <div class="text col-sm-12 <?php if (isset($social_media)) { ?>col-lg-8<?php } else { ?>col-lg-12<?php } ?>">
        <?php foreach ($data['topic']['content'] as $content) {
            if ($content['content_type_key'] == 'infotext') {
                foreach ($content['element_items'] as $element) {
                    echo ci()->get_view('common/element_types', array("element" => $element));
                    if (@$element['mobile'] != 1) {
                        $show_more = true;
                    }
                 }
            }
        }
        if (@$show_more) { ?>
        <div class="show_more">
            <button class="btn-clear-blue btn-sm show-full-foldpage hidden-lg" onclick="showFullFoldpage(event)">
                mehr...
            </button>
        </div>
        <?php
        } ?>
    </div>

    <div class="picture no-margin-top box col-sm-12 col-lg-4">
        <?php if (isset($data['topic']['picture'])) {
            $max_picture_width = 800;
            ?>
            <?php foreach ($data['topic']['picture'] as $picture ) {
             } ?>
            <img src="<?= Media_lib::url(@$picture, @$max_picture_width) ?>">
        <?php } ?>
        <?php if (isset($social_media)){ ?>
        <div class="social-media">
<?php foreach ($data['topic']['content'] as $content) {
    if ($content['content_type_key'] == 'socialmedia') {
        foreach ($content['element_items'] as $element) {  ?>
            <a href="<?= $element['text'] ?>" target="_blank">
                 <img src="<?= site_url('assets/img/icons/' . $element['element_type_key'] . '.png')?>">
            </a>
           <?php }
                }
            } ?>
        </div>
        <?php } ?>
    </div>
</div>


<?php

/**
 * User: sbraun
 * Date: 27.03.17
 * Time: 17:22
 * lastChange: 2018-04-12 13:08
 */
class Cached_json_model extends MY_Model
{
    #public $database_group = 'smr_frontend';

    public $db_fields = array("id", "key", "params", "value", "created", "modified");
//    public $database_group = 'smr-picture';
    public $table = "cached_json";
    public $table_redirects = "cached_redirects";
    public $table_searchs = "cached_searchs";
//    public $table2 = "file";
//    public $table3 = "directory";
//    public $table4 = "picture_relation";
//	public $read_only = true;

//    public $full_controller_name = "cms/picture";
//    public $item_label = "Bild";
//    public $items_label = "Bilder";
//    public $linked_by_knot = array("gallery","element","location","hotel","producer");

    public $item_class = "Cached_item";

    public $expire_time = 24*60*60;#24 Stunden

    /** @var bool use cache without timelimit */
    public $use_cache = false;

    public function __construct() {
        parent::__construct();
//        $this->garbage_collector();
        $this->use_cache = @ci()->config->item("use_cache");
    }

    private function get_table($key):string {
        if (strpos($key, '/public/api/check_redirect_url') !== false) {
            return $this->db()->database . '.' . $this->table_redirects;
        } elseif (strpos($key, '/search/search/') !== false) {
            return $this->db()->database . '.' . $this->table_searchs;
        } else {
            return $this->db()->database . '.' . $this->table;
        }
    }

    /**
     * @param string $key
     * @param mixed|array|string|null $params (non-json)
     * @return Cached_item|false
     */
    public function get_cached_item(string $key = "package", $params = null) {
        $params_json = json_encode($params);
        $db = $this->db();
        $db->select("unix_timestamp() as ts_now", false);
        $db->select("unix_timestamp(modified) as ts_modified", false);
        $db->select("unix_timestamp() - unix_timestamp(modified) as age", false);
        $db->select($this->get_table($key) . ".*");
        $items = $this->get_items_by_fields(['key' => $key, "params" => $params_json], null, $this->get_table($key), $this->item_class);
        if ($items)
            return $items[0];
        else
            return false;
    }

    /**
     * @param string $key
     * @param mixed|array|string|null $params (non-json)
     * @param array|string(json)|null $value
     * @param null $data params like ['expire' => 60*60]
     * @return array|bool
     */
    public function save_cached_item(string $key = "package", $params = null, $value = null, $data = null) {
        $data2 = [
//            "id" => @$data->id,
            "key" => $key,
            "params" => json_encode($params),
            "value" => (is_string($value))? $value : json_encode($value),
        ];
        if (isset($data['expire']))
            $data2['expire'] = $data['expire'];
        $db = $this->db();
//        $db->set("created", "now()", false);
        $db->set("modified", "now()", false);
        $r = $db->replace($this->get_table($key), $data2);
//        return parent::save_one_item2($data2);
        return $r;
    }

    /**
     * @param string $key
     * @param mixed $params (expect non-json-data)
     * @param MY_DB_query_builder $db
     * @return bool|void
     */
    public function delete_cached_item(string $key, $params = null, $db = null, $type = null) {
//        if (is_null($db))
//            $db = $this->db();
//        $item = (object) ["key" => $key, "params" => json_encode($params)];
//        $r = $db->where((array) $item)->delete($this->table);
//        return $r;

        if (is_null($db))
            $db = $this->db();

        if ($type == 'redirect') {
            $params = rawurlencode($params);
            $r = $db->where("key like '%$key%' and params like '%$params%'")->delete($this->get_table($key));
        } else {
            $r = $db->where("key like '%$key/$params%'")->delete($this->get_table($key));
        }
        return $r;

    }
    public function garbage_collector() {
        # remove old
        $max_age_to_hold = 30*24*60*60;# 30 days
        $db = $this->db();
        $db->where("(unix_timestamp() - unix_timestamp(modified)) > $max_age_to_hold");
        $db->delete($this->table);
        # set created timestamp
        $db->set("created", "modified", false);
        $db->where("created IS NULL", null, false);
        $db->update($this->table);
    }

    /* Aliasses ***********************************************/

    public function save($cache_key, $value, $cache_time) {
        return $this->save_cached_item($cache_time, null, $value, null);
    }

}

class Cached_item extends Item
{
    /** @var string */
    private $json;
    /** @var array */
    private $decoded;
    /**
     * @return bool true|false true: cached item can and should be used, false: generate new item, do not use cached one
     */
    public function is_valid() {
        if (ci()->cached_json_model()->use_cache == false)
            return false;
        $expire_time = ($this->expire)?: ci()->cached_json_model()->expire_time;
        if (ci()->cached_json_model()->use_cache && $this->age <= $expire_time)
            return true;
        else
            return false;
    }

    public function get_json() {
        if (property_exists($this, 'json') && !is_null($this->json)) {
        } else {
            $this->json = $this->value;
        }
        return $this->json;
    }

    public function get_decoded() {
        if (property_exists($this, 'decoded') && !is_null($this->decoded)) {
        } else {
            $this->decoded = json_decode($this->value, true);
        }
        return $this->decoded;
    }

    public function set_value($new_value) {
        $this->value = $new_value;
        $this->decoded = null;
        $this->json = null;
    }

    /**
     * Alias for get_decoded()
     * @deprecated use get_decoded()
     * @return mixed
     */
    public function decoded() {
        return $this->get_decoded();
    }

}
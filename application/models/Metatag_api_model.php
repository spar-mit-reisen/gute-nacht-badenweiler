<?php

/**
 * User: sbraun
 * Date: 15.03.17
 * Time: 13:14
 */
class Metatag_api_model extends MY_Model
{
    public $db = false;
    public $load_db = false;

    public function get_metatags($object_type, $object_id, $type = "all") {
        $url_segments = ['getMetatag', $object_type, $object_id, $type];
        $get_params = [];
        $post_params = [];
        $api_data_string = ci()->fake_api_lib()->api_return($url_segments, [], $get_params, $post_params);
        $api_data = json_decode($api_data_string);
//        var_dump($api_data);
//        die;
        if (!$api_data || empty($api_data_string)) {
            echo "Fehler in Api-Antwort: \n<pre>";
            var_dump(ci()->curl_lib()->error);
            echo "Response:<br><hr>\n";
            print_r($api_data_string);
            echo "\n</pre><hr>";
        } else {
            if (is_array(@$api_data->metatags) || is_object(@$api_data->metatags)) {
                if (!empty($api_data->metatags)) {
                    return $api_data->metatags;
                } else
                    return $api_data->metatags;
            } else
                return $api_data;
        }
    }
}
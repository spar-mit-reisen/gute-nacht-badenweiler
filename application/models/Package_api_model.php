<?php

/**
 * Created by PhpStorm.
 * User: sbraun
 * Date: 06.03.17
 * Time: 13:19
 */
class Package_api_model extends MY_Model {

    public $db = false;
    public $load_db = false;
    public $limit_num = 15;

    public function getTopicPackageMeta($topic_id, $filter_for_api = null) {
        if (@ci()->config->item("use_fake_api")) {
            $api_data = ci()->fake_api_lib()->getTopicPackages($topic_id, "array");
        } else {
            $api_data_string = ci()->fake_api_lib()->api_return(['getTopicPackageMeta', $topic_id], $filter_for_api);
            $api_data = json_decode($api_data_string, true);
        }

        if (!is_array($api_data)) {
            var_dump(ci()->curl_lib->error);
            var_dump($api_data_string);
        }
        return $api_data;
    }

}

<?php
/**
 * User: sbraun
 * Date: 29.03.17
 * Time: 16:41
 */

$db['default_webdev'] = array(
    'dsn'	=> '',
    'hostname' => '192.168.1.223',
    'username' => 'root',
    'password' => 'ciafbi',
    'database' => 'smr_db',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['smr_release_test'] = [
    'dsn'	=> '',
    'hostname' => '127.0.0.1',
    'username' => 'smr_dev',
    'password' => 'oD60@a2sMv6q!q58',
    'database' => 'smr_release_test',
//    'hostname' => '176.28.13.236',
//    'username' => 'root',
//    'password' => 'root',
//    'database' => 'smr_live_public',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => TRUE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE,
    'options' => array(PDO::ATTR_TIMEOUT => 5),
];

$db['smr-picture_org'] = array(
    'dsn'	=> '',
    'hostname' => '192.168.1.132',
    'username' => 'root',
    'password' => '',
    'database' => 'smrdms',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => TRUE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE,
    'options' => array(PDO::ATTR_TIMEOUT => 5),
);

$db['smr-picture_copy'] = array(
    'dsn'	=> '',
    'hostname' => '192.168.1.223',
    'username' => 'root',
    'password' => 'ciafbi',
    'database' => 'smr_dms_copy',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => TRUE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => $db['smr-picture_org'],
    'save_queries' => TRUE,
    'options' => array(PDO::ATTR_TIMEOUT => 5),
);



// sb: unused? old shid from swen?
//$db['smr-package-manager'] = array(
//    'dsn'	=> '',
//    'hostname' => '192.168.1.223',
//    'username' => 'root',
//    'password' => 'ciafbi',
//    'database' => 'dubai',
//    'dbdriver' => 'mysqli',
//    'dbprefix' => '',
//    'pconnect' => FALSE,
//    'db_debug' => (ENVIRONMENT !== 'production'),
//    'cache_on' => FALSE,
//    'cachedir' => '',
//    'char_set' => 'utf8',
//    'dbcollat' => 'utf8_general_ci',
//    'swap_pre' => '',
//    'encrypt' => FALSE,
//    'compress' => FALSE,
//    'stricton' => FALSE,
//    'failover' => array(),
//    'save_queries' => TRUE
//);


/* sample for localhost.php : */
$db['cred'] = [
//    'username' => 'root',
//    'password' => 'root',
];

$db['default_localhost'] = array_merge(
    $db['mysql_default'],
    $db['default_webdev'],
//    $db['cred'],
    [
//        'hostname' => '127.0.0.1',
    ]
);

$db['smr_release_test'] = array_merge(
    $db['mysql_default'],
    $db['smr_release_test'],
//    $db['cred'],
    [
//    'hostname' => '127.0.0.1',
        'options' => array(PDO::ATTR_TIMEOUT => 5),
    ]
);

$db['smr-picture_localhost'] = array_merge(
    $db['mysql_default'],
    $db['smr-picture_org'],
//    $db['cred'],
    [

    ]
);

$db['smr_frontend'] = array_merge(
    $db['mysql_default'],
    $db['default_webdev'],
    $db['cred'],
    [
        'database' => 'smr_frontend',
    ]
);
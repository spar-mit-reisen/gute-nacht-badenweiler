<?php
/**
 * User: sbraun
 * Date: 29.03.17
 * Time: 16:41
 */

# HOMEOFFICE
if (0) {
//    require_once __DIR__."/entwicklung.php";


    $db['cred'] = [
        'username' => 'root',
        'password' => 'root',
    ];

    $db['default_localhost'] = array_merge(
        $db['mysql_default'],
        $db['default_webdev'],
        $db['cred'],
        [
            'cache_on' => false,
            'hostname' => '127.0.0.1',
        ]
    );

    $db['smr_release_test'] = array_merge(
        $db['mysql_default'],
        $db['smr_release_test'],
        $db['cred'],
        [
            'database' => 'smr_release_test',
            'cache_on' => TRUE,
            'hostname' => '127.0.0.1',
            'options' => array(PDO::ATTR_TIMEOUT => 5),

        ]
    );

    $db['smr-picture_localhost'] = array_merge(
        $db['mysql_default'],
        $db['smr-picture_copy'],
        $db['cred'],
        [
            'cache_on' => TRUE,
            'hostname' => '127.0.0.1',
            'database' => 'smr_dms_copy',
        ]
    );


//    $db['smr-picture_localhost'] = array(
//        'dsn'	=> '',
//        'hostname' => '83.169.6.138',
//        'username' => 'smr_cms',
//        'password' => 'oD60@a2sMv6q!q58',
//        'database' => 'smr_dms_copy',
//        'dbdriver' => 'mysqli',
//        'dbprefix' => '',
//        'pconnect' => TRUE,
//        'db_debug' => (ENVIRONMENT !== 'production'),
//        'cache_on' => FALSE,
//        'cachedir' => '',
//        'char_set' => 'utf8',
//        'dbcollat' => 'utf8_general_ci',
//        'swap_pre' => '',
//        'encrypt' => FALSE,
//        'compress' => FALSE,
//        'stricton' => FALSE,
//        'failover' => array(),
//        'save_queries' => FALSE,
//        'options' => array(PDO::ATTR_TIMEOUT => 5),
//    );

} else {
    $db['cred'] = [
        'save_queries' => FALSE,
    ];

    $db['default_localhost'] = array_merge(
        $db['mysql_default'],
        $db['default_webdev'],
        $db['cred'],
        [
            'database' => 'smr_frontend'
        ]
    );

    $db['smr_release_test'] = array_merge(
        $db['mysql_default'],
        $db['smr_release_test'],
        $db['cred'],
        [
            'options' => array(PDO::ATTR_TIMEOUT => 5),
        ]
    );

    $db['smr-picture_localhost'] = array_merge(
        $db['mysql_default'],
        $db['smr-picture_org'],
        $db['cred'],
        [
        ]
    );

}

ini_set("display_errors", 1);
error_reporting(E_ALL);
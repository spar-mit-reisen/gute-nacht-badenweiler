function toggleMobileMenu() {
    $('header .burger button').toggleClass('has-menu');
    $('.mobile-menu-wrap').toggle().promise().done(function() {
        if($(this).is(':visible')) {
            $('body').css('overflow-y', 'hidden');
        } else {
            $('body').css('overflow-y', 'auto');
        }
    });

    if ($(".mobile-menu").find("a").hasClass("active")) {
    var $container = $(".mobile-menu-wrap");
    var $scrollTo = $('.active-parent').closest( "li" );
    $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop(), scrollLeft: 0},0);
    }
}

function toggleMobileSubmenu(event) {
    $(event.currentTarget).closest('.row').find('.arrow').toggleClass('open');
    $(event.currentTarget).closest('li').find('ul:first').slideToggle('slow', 'easeInOutBack');
}





$(document).ajaxStart(function () {
    NProgress.start();
});

$(document).ajaxComplete(function () {
    NProgress.done();
});

function scrollTop() {
    $('html, body').animate({scrollTop: 0}, 600);
}

function scrollToElement(element) {
    var offset = $(element).offset().top;
    $('html, body').animate({scrollTop: parseInt(offset + 25)}, 200);
}

function toggleEmailRequired() {
    var input = $('input[name="email"]');
    var label = $('label[for="email"]');
    if (input.attr('required')) {
        input.attr('required', false);
        label.html('E-Mail');
    } else {
        input.attr('required', true);
        label.html('E-Mail <strong class="text-red">*</strong>');
    }
}

function slick_init_carousel() {
    var selector = '.carousel.is-not-inited';
    $(selector).slick();
    $(selector + ' .carousel-inner').css('display', 'flex');
    $(selector).removeClass('is-not-inited');
}

function photoswipe_init_falte() {
    var $pswp = $('.pswp')[0];
    var image = [];
    var selector = '.gallery-wrap.is-not-inited';

    $(selector).each(function() {
        var $pic = $(this);
        var getItems = function() {
            var items = [];
            $pic.find('.gallery-images div').each(function() {
                var item = {
                    src: $(this).data('path'),
                    w: $(this).data('width'),
                    h: $(this).data('height'),
                    title: $(this).data('title')
                };
                items.push(item);
            });
            return items;
        };
        var items = getItems();

        $pic.click(selector, function(event) {
            event.preventDefault();
            var galleryId = $(this).data('gallery-id');
            var index = $(this).find('.carousel').slick('slickCurrentSlide');
            var options = {
                galleryUID: 'gallery_'+galleryId,
                index: index,
                bgOpacity: 1
            };

            var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
            lightBox.init();
        });
    });

    $(selector).removeClass('is-not-inited');
}


/**
 * @author Sebastian & Philip
 * @param {element or selector} form
 * @returns {getDataObjectByForm.data}
 */
function getDataObjectByForm(form) {
    form = $(form);
    var data = {
        request_type: 'ajax',
        date: new Date().valueOf() /* to avoid request caching */
    };
    /* do not use browser cache */
    if (form.data('nocache') !== undefined && form.data('nocache')) {
        data.date = data.date;
    } else { /* allow browser cache */
        data.date = "";
    }
    if (form.data('additional_fields') !== undefined) {
        var container2 = $(form.data('additional_fields'));
        for (i = 0; i < $(':input', container2).length; i++) {
            data[$($(':input', container2)[i]).attr("name")] = $($(':input', container2)[i]).val();
        }
    }
    if (form.data('additional_fields2') !== undefined) {
        var container3 = $(form.data('additional_fields2'));
        for (var x = 0; x < $(':input', container3).length; x++) {
            data[$($(':input', container3)[x]).attr("name")] = $($(':input', container3)[x]).val();
        }
    }
    if (form.data('limit_from') !== undefined)
        data.limit_from = form.data('limit_from');
    if (form.data('limit_num') !== undefined)
        data.limit_num = form.data('limit_num');
    if (form.data('page') !== undefined)
        data.page = form.data('page');
    if (form.data('sort') !== undefined)
        data.sort = form.data('sort');
    if (form.data('month') !== undefined)
        data.month = form.data('month');
    if (form.data('start_date') !== undefined)
        data.start_date = form.data('start_date');
    if (form.data('end_date') !== undefined)
        data.end_date = form.data('end_date');
    for (var i = 0; i < $(':input', form).length; i++) {
        data[$($(':input', form)[i]).attr('name')] = $($(':input', form)[i]).val();
    }
    return data;
}

function jsonToDom(json) {
    var selector, element;
    var time = 300;
    var i = 0;
    if (json.html !== undefined) {

        if (json.html.append !== undefined) {
            for (selector in json.html.append) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                element = $(json.html.append[selector]).hide();
                $(selector).delay(i++ * time).append(element);
                /* jshint loopfunc:true */
                element.fadeIn(function () {
                    $(this).removeClass('is-not-faded');
                    if (json.html.callback !== undefined) {
                        var callbackFunction = window[json.html.callback];
                        callbackFunction();
                    }
                });
            }
        }
        if (json.html.prepend !== undefined) {
            for (selector in json.html.prepend) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                element = $(json.html.prepend[selector]).hide();
                $(selector).delay(i++ * time).prepend(element);
                element.fadeIn();
            }
        }
        if (json.html.insertAfter !== undefined) {
            for (selector in json.html.insertAfter) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                element = $(json.html.insertAfter[selector]).hide();
                $(element).delay(i++ * time).insertAfter(selector);
                /* jshint loopfunc:true */
                element.fadeIn(function () {
                    $(this).removeClass('is-not-faded');
                    if (json.html.callback !== undefined) {
                        var callbackFunction = window[json.html.callback];
                        callbackFunction();
                    }
                });
            }
        }
        if (json.html.insertBefore !== undefined) {
            for (selector in json.html.insertBefore) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                element = $(json.html.insertBefore[selector]).hide();
                $(element).delay(i++ * time).insertBefore(selector);
                /* jshint loopfunc:true */
                element.fadeIn(function () {
                    $(this).removeClass('is-not-faded');
                    if (json.html.callback !== undefined) {
                        var callbackFunction = window[json.html.callback];
                        callbackFunction();
                    }
                });
            }
        }
        if (json.html.replace !== undefined) {
            for (var selectorReplace in json.html.replace) {
                replace(selectorReplace, json.html.replace[selectorReplace], fadeTime);
            }
        }
        if (json.html.html !== undefined) {
            for (selector in json.html.html) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                $(selector).delay(i++ * time).html(json.html.html[selector]).fadeIn();
            }
        }
        if (json.html.fadeIn !== undefined) {
            for (selector in json.html.fadeIn) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                $(selector).delay(i++ * time).fadeIn();
            }
        }
        if (json.html.fadeOut !== undefined) {
            for (selector in json.html.fadeOut) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                $(selector).delay(i++ * time).fadeOut();
            }
        }
        if (json.html.show !== undefined) {
            for (selector in json.html.show) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                $(selector).delay(i++ * time).show();
            }
        }
        if (json.html.hide !== undefined) {
            for (selector in json.html.hide) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                $(selector).delay(i++ * time).hide();
            }
        }
        if (json.html.remove !== undefined) {
            for (selector in json.html.remove) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                $(selector).delay(i++ * time).remove(selector).fadeOut();
            }
        }
        if (json.html.addClass !== undefined) {
            for (selector in json.html.addClass) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                $(selector).delay(i++ * time).addClass(json.html.html[selector]);
            }
        }
        if (json.html.removeClass !== undefined) {
            for (selector in json.html.removeClass) {
                if ($(selector).length === 0) console.log("invalid selector: " + selector);
                $(selector).delay(i++ * time).removeClass(json.html.html[selector]);
            }
        }
        if (json.html.attribute !== undefined) {
            for (selector in json.html.attribute) {
                // $(selector).delay(i++ * time).attr('data-month', json.html.attribute[selector]);
                $(selector).delay(i++ * time).data('month', json.html.attribute[selector]);
            }
        }
        if (json.html.href !== undefined) {
            for (selector in json.html.href) {
                $(selector).delay(i++ * time).attr("href", json.html.href[selector]);
            }
        }
    }
}


function loadMorePackages() {
    var action_2 = scope.site_url + 'Search/getMorePackages';
    var options = {};
    options.counterNow = parseInt($("#desktopCounterValue").val());


    options.counterPosition = $("#desktopCounterValuePos").val();
    var jqxhr = $.post(action_2, {'detailsearch[]': JSON.stringify(options)})
        .done(function (response) {
            $counter = 0;
            var arr;
            if (Array.isArray(response)||typeof response==="object"){
                arr = response;
            }

            else{
                arr = $.parseJSON(response);
            }
            arr[0].forEach(function (item) {

                $("#packagestripes").append(item);
            });
            appendCounterDesktopSearch(arr[1], arr[2]);
        })
        .fail(function (jqXHR, textStatus, error) {
            console.log("Post error: " + error);
        });

}


function ajax_submit(form) {
    $('body').css('cursor', 'progress');
    var url = $(form).attr('action');
    if (url === undefined) {
        url = $(form).data('action');
    }
    if (url === undefined) {
        url = $(form).attr('href');
    }
    if (url === undefined) {
        console.log('selector invalid: ' + form);
        return false;
    }
    var data = getDataObjectByForm(form);


    var jqxhr = $.getJSON(url, data)
        .done(function (json) {
            jsonToDom(json);
            if (json.reload !== undefined && json.reload) {
                location.reload();
            }
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ', ' + error;
            console.log('Request Failed: ' + err);
        });
    $('body').css('cursor', 'default');
    return false;
}

function ajax_data(url, data) {
    $("body").css("cursor", "progress");
    var jqxhr = $.post(url, data, function () {
    }, 'json')
        .done(function (response) {
            var json;
            if (response instanceof Object) {
                json = response;
            } else {
                json = $.parseJSON(response);
            }
            jsonToDom(json);
            if (json.reload !== undefined && json.reload)
                location.reload();
            $("body").delay(1000).css("cursor", "default");
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
            //alert("Fehler!");
        });
    return false;
}

function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + '; expires=' + expires.toUTCString() + '; path=/';
}


function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}

function deleteCookie(key) {
    if (getCookie(key)) {
        document.cookie = key + '=""; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/';
    }
}

function get_cookies_array() {

    var cookies = {};

    if (document.cookie && document.cookie !== '') {
        var split = document.cookie.split(';');
        for (var i = 0; i < split.length; i++) {
            var name_value = split[i].split("=");
            name_value[0] = name_value[0].replace(/^ /, '');
            cookies[decodeURIComponent(name_value[0])] = decodeURIComponent(name_value[1]);
        }
    }

    return cookies;
}

function backToTopButton() {
    // Der Button wird mit JavaScript erzeugt und vor dem Ende des body eingebunden.
    var back_to_top_button = ['<a href="#top" class="back-to-top"><i class="fa fa-chevron-circle-up"></i>  nach oben</a>'].join("");
    $("body").append(back_to_top_button);

    // Der Button wird ausgeblendet
    $(".back-to-top").hide();

    // Funktion für das Scroll-Verhalten
    $(function () {
        $(document).scroll(function () {
            if ($(this).scrollTop() > 300) { // Wenn 100 Pixel gescrolled wurde
                $('.back-to-top').fadeIn();
            } else {
                $('.back-to-top').fadeOut();
            }
        });

        $('.back-to-top').click(function () { // Klick auf den Button
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });
}

function showFullFoldpage(event) {
    $('.element-wrap.hidden-sm').fadeIn();
    $('.show-full-foldpage').hide();
}

